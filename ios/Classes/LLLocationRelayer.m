//
//  LocationRelayer.m
//  localytics
//
//  Created by John Rikard Nilsen on 19.10.20.
//

#import "LLLocationRelayer.h"
#import <Localytics/Localytics.h>
#import "LLLocalyticsTranslator.h"
#import <Flutter/Flutter.h>

@implementation LLLocationRelayer

- (instancetype)initWithChannel:(FlutterMethodChannel*)channel {
    _channel = channel;
    return self;
}

- (void)localyticsDidUpdateLocation:(CLLocation *)location {
    [_channel invokeMethod:@"localyticsDidUpdateLocation" arguments:[LLLocalyticsTranslator toLocationMap:location]];
}

- (void)localyticsDidUpdateMonitoredRegions:(NSArray<LLRegion *> *)addedRegions removeRegions:(NSArray<LLRegion *> *)removedRegions {
    NSMutableArray *args = [NSMutableArray new];
    [args addObject:[LLLocalyticsTranslator toListOfRegionMap:addedRegions]];
    [args addObject:[LLLocalyticsTranslator toListOfRegionMap:removedRegions]];
    [_channel invokeMethod:@"localyticsDidUpdateMonitoredRegions" arguments:args];
}

- (void)localyticsDidTriggerRegions:(NSArray<LLRegion *> *)regions withEvent:(LLRegionEvent)event {
    NSMutableArray *args = [NSMutableArray new];
    [args addObject:[LLLocalyticsTranslator toListOfRegionMap:regions]];
    [args addObject:[LLLocalyticsTranslator toRegionEventString:event]];
    [_channel invokeMethod:@"localyticsDidTriggerRegions" arguments:args];
}

@end
