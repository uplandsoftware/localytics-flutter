//
//  LocalyticsTranslator.m
//  localytics
//
//  Created by John Rikard Nilsen on 21.10.20.
//

#import "LLLocalyticsTranslator.h"
@import Localytics;

@implementation LLLocalyticsTranslator
+(NSDictionary *)toRegionMap:(LLRegion *)region {
    NSMutableDictionary *rawRegion = [NSMutableDictionary new];
    rawRegion[@"name"] = region.name;
    rawRegion[@"attributes"] = region.attributes;
    rawRegion[@"uniqueId"] = region.region.identifier;
    rawRegion[@"latitude"] = [NSNumber numberWithDouble:region.region.center.latitude];
    rawRegion[@"longitude"] = [NSNumber numberWithDouble:region.region.center.longitude];
    rawRegion[@"radius"] = [NSNumber numberWithInt:region.region.radius];
    rawRegion[@"enterAnalyticsEnabled"] = [NSNumber numberWithBool:region.region.notifyOnEntry];
    rawRegion[@"exitAnalyticsEnabled"] = [NSNumber numberWithBool:region.region.notifyOnExit];
    return rawRegion;
}

+(NSArray<NSDictionary *> *)toListOfRegionMap:(NSArray<LLRegion *> *)regions {
    NSMutableArray<NSDictionary *> *rawRegions = [NSMutableArray new];
    for(LLRegion *region in regions) {
        [rawRegions addObject:[LLLocalyticsTranslator toRegionMap:region]];
    }
    return rawRegions;
}

+(LLRegionEvent)toRegionEvent:(NSString *)raw {
    if ([raw isEqualToString:@"enter"]) {
        return LLRegionEventEnter;
    } else {
        return LLRegionEventExit;
    }
}

+ (NSString *)toRegionEventString:(LLRegionEvent)raw {
    if (raw == LLRegionEventEnter) {
        return @"enter";
    } else {
        return @"exit";
    }
}

+(NSDictionary *)toLocationMap:(CLLocation *)location {
    NSMutableDictionary *map = [NSMutableDictionary new];
    map[@"latitude"] = @(location.coordinate.latitude);
    map[@"longitude"] = @(location.coordinate.longitude);
    map[@"altitude"] = @(location.altitude);
    map[@"time"] = @(location.timestamp.timeIntervalSince1970);
    map[@"horizontalAccuracy"] = @(location.horizontalAccuracy);
    map[@"verticalAccuracy"] = @(location.verticalAccuracy);
    return map;
}

+ (NSDictionary *)toCampaignDictionary:(LLCampaignBase *)campaign {
    if ([campaign isKindOfClass:[LLInAppCampaign class]]) {
           return [self toInAppCampaignDictionary:(LLInAppCampaign *) campaign];
       } else if ([campaign isKindOfClass:[LLInboxCampaign class]]) {
           return [self toInboxCampaignDictionary:(LLInboxCampaign *)campaign];
       } else if ([campaign isKindOfClass:[LLPlacesCampaign class]]) {
           return [self toPlacesCampaignDictionary:(LLPlacesCampaign *) campaign];
       }
       return nil;
}

+(NSDictionary*)toInAppCampaignDictionary:(LLInAppCampaign *)campaign {
    NSString *typeString = @"";
    switch (campaign.type) {
        case LLInAppMessageTypeTop:
            typeString = @"top";
            break;
        case LLInAppMessageTypeBottom:
            typeString = @"bottom";
            break;
        case LLInAppMessageTypeCenter:
            typeString = @"center";
            break;
        case LLInAppMessageTypeFull:
            typeString = @"full";
            break;
    }
    NSString* buttonLocation;
    if (campaign.dismissButtonLocation == LLInAppMessageDismissButtonLocationLeft) {
        buttonLocation = @"left";
    } else {
        buttonLocation = @"right";
    }
    return @{
             // LLCampaignBase
             @"campaignId": @(campaign.campaignId),
             @"name": campaign.name,
             @"attributes": campaign.attributes ?: [NSNull null],

             // LLWebViewCampaign
             @"creativeFilePath": campaign.creativeFilePath ?: [NSNull null],

             // LLInAppCampaign
             @"type": typeString,
             @"isResponsive": @(campaign.isResponsive),
             @"aspectRatio": @(campaign.aspectRatio),
             @"offset": @(campaign.offset),
             @"backgroundAlpha": @(campaign.backgroundAlpha),
             @"dismissButtonHidden": @(campaign.isDismissButtonHidden),
             @"dismissButtonLocation": buttonLocation,
             @"eventName": campaign.eventName,
             @"eventAttributes": campaign.eventAttributes ?: [NSNull null],
             @"classType": @"InAppCampaign"
             };
}

+(NSDictionary*)toPlacesCampaignDictionary:(LLPlacesCampaign *)campaign {
    return @{
             // LLCampaignBase
             @"campaignId": @(campaign.campaignId),
             @"name": campaign.name,
             @"attributes": campaign.attributes ?: [NSNull null],
             
             // LLPlacesCampaign
             @"message": campaign.message,
             @"soundFilename": campaign.soundFilename ?: [NSNull null],
             @"region": [LLLocalyticsTranslator toRegionMap:campaign.region],
             @"triggerEvent": campaign.event == LLRegionEventEnter ? @"enter" : @"exit",
             @"category": campaign.category,
             @"attachmentURL":campaign.attachmentURL,
             @"attachmentType":campaign.attachmentType,
             @"classType": @"PlacesCampaign"
             };
}

+ (NSDictionary *)toInboxCampaignDictionary:(LLInboxCampaign *)campaign {
    return @{
        @"campaignId": [NSNumber numberWithLong:campaign.campaignId],
        @"ruleName": campaign.name,
        @"attributes": campaign.attributes,
        @"read": [NSNumber numberWithBool:campaign.isRead],
        @"deleted": [NSNumber numberWithBool:campaign.isDeleted],
        @"isPushToInboxCampaign": [NSNumber numberWithBool:campaign.isPushToInboxCampaign],
        @"title": campaign.titleText,
        @"sortOrder": [NSNumber numberWithLong:campaign.sortOrder],
        @"receivedDate": [NSNumber numberWithDouble:campaign.receivedDate],
        @"summary": campaign.summaryText,
        @"hasCreative": [NSNumber numberWithBool:campaign.hasCreative],
        @"deepLinkUrl": [self guardNil:campaign.deepLinkURL],
        @"thumbnailUri": [self guardNil:campaign.thumbnailUrl],
        @"classType": @"InboxCampaign"
    };
}
+(id)guardNil:(id)arg {
    return arg ? arg : [NSNull null];
}
@end
