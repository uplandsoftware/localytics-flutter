//
//  LLCallToActionRelayer.h
//  localytics
//
//  Created by John Rikard Nilsen on 13.11.20.
//

#import <Localytics/Localytics.h>
#import <Flutter/Flutter.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LLCallToActionRelayer : NSObject<LLCallToActionDelegate>
@property (strong, nonatomic) FlutterMethodChannel *channel;
- (instancetype)initWithChannel:(FlutterMethodChannel *)channel;

@end

NS_ASSUME_NONNULL_END
