//
//  LLCallToActionRelayer.m
//  localytics
//
//  Created by John Rikard Nilsen on 13.11.20.
//

#import "LLCallToActionRelayer.h"
#import "LLLocalyticsTranslator.h"

@implementation LLCallToActionRelayer

- (instancetype)initWithChannel:(FlutterMethodChannel *)channel {
    self.channel = channel;
    return self;
}

- (BOOL)localyticsShouldDeeplinkToSettings:(LLCampaignBase *)campaign {
    [_channel invokeMethod:@"localyticsShouldDeeplinkToSettings" arguments:[LLLocalyticsTranslator toCampaignDictionary:campaign]];
    return YES;
}

- (BOOL)localyticsShouldDeeplink:(NSURL *)url campaign:(LLCampaignBase *)campaign {
    NSDictionary *body = [NSDictionary new];
    [body setValue:url forKey:@"url"];
    [body setValue:[LLLocalyticsTranslator toCampaignDictionary:campaign] forKey:@"campaign"];
    [_channel invokeMethod:@"localyticsShouldDeeplink" arguments:body];
    return YES;
}

- (void)localyticsDidOptOut:(BOOL)optedOut campaign:(LLCampaignBase *)campaign {
    NSDictionary *body = [NSDictionary new];
    [body setValue:[NSNumber numberWithBool:optedOut] forKey:@"optedOut"];
    [body setValue:[LLLocalyticsTranslator toCampaignDictionary:campaign] forKey:@"campaign"];
    [_channel invokeMethod:@"localyticsDidOptOut" arguments:body];
}

- (void)localyticsDidPrivacyOptOut:(BOOL)privacyOptedOut campaign:(LLCampaignBase *)campaign {
    NSDictionary *body = [NSDictionary new];
    [body setValue:[NSNumber numberWithBool:privacyOptedOut] forKey:@"optedOut"];
    [body setValue:[LLLocalyticsTranslator toCampaignDictionary:campaign] forKey:@"campaign"];
    [_channel invokeMethod:@"localyticsDidPrivacyOptOut" arguments:body];
}

- (BOOL)localyticsShouldPromptForLocationWhenInUsePermissions:(LLCampaignBase *)campaign {
    [_channel invokeMethod:@"localyticsShouldPromptForLocationWhenInUsePermissions" arguments:[LLLocalyticsTranslator toCampaignDictionary:campaign]];
    return YES;
}

- (BOOL)localyticsShouldPromptForLocationAlwaysPermissions:(LLCampaignBase *)campaign {
    [_channel invokeMethod:@"localyticsShouldPromptForLocationAlwaysPermissions" arguments:[LLLocalyticsTranslator toCampaignDictionary:campaign]];
    return YES;
}

- (BOOL)localyticsShouldPromptForNotificationPermissions:(LLCampaignBase *)campaign {
    [_channel invokeMethod:@"localyticsShouldPromptForNotificationPermissions" arguments:[LLLocalyticsTranslator toCampaignDictionary:campaign]];
    return YES;
}

@end
