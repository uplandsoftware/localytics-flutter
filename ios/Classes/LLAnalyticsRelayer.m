//
//  AnalyticsRelayer.m
//  localytics
//
//  Created by John Rikard Nilsen on 13.10.20.
//

#import "LLAnalyticsRelayer.h"
#import <Localytics/Localytics.h>
#import <Flutter/Flutter.h>

@implementation LLAnalyticsRelayer

- (instancetype)initWithChannel:(FlutterMethodChannel*)channel {
    _channel = channel;
    return self;
}

// MARK: - LLAnalyticsDelegeate methods
- (void)localyticsSessionWillOpen:(BOOL)isFirst isUpgrade:(BOOL)isUpgrade isResume:(BOOL)isResume;
{
    NSMutableArray *args = [NSMutableArray new];
    [args addObject:[NSNumber numberWithBool:isFirst]];
    [args addObject:[NSNumber numberWithBool:isUpgrade]];
    [args addObject:[NSNumber numberWithBool:isResume]];
    [_channel invokeMethod:@"localyticsSessionWillOpen" arguments:args];
}

- (void)localyticsSessionDidOpen:(BOOL)isFirst isUpgrade:(BOOL)isUpgrade isResume:(BOOL)isResume;
{
    NSMutableArray *args = [NSMutableArray new];
    [args addObject:[NSNumber numberWithBool:isFirst]];
    [args addObject:[NSNumber numberWithBool:isUpgrade]];
    [args addObject:[NSNumber numberWithBool:isResume]];
    [_channel invokeMethod:@"localyticsSessionDidOpen" arguments:args];
}

- (void)localyticsDidTagEvent:(NSString *)event attributes:(NSDictionary *)attributes customerValueIncrease:(NSNumber *)customerValueIncrease
{
    NSMutableArray *args = [NSMutableArray new];
    [args addObject:event];
    if (attributes != nil) {
        [args addObject:attributes];
    }
    if (customerValueIncrease != nil) {
        [args addObject:customerValueIncrease];
    }
    [_channel invokeMethod:@"localyticsDidTagEvent" arguments:args];
}

- (void)localyticsSessionWillClose{
    [_channel invokeMethod:@"localyticsSessionWillClose" arguments:nil];
}
@end
