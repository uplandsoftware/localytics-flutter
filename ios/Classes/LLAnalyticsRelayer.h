//
//  AnalyticsRelayer.h
//  localytics
//
//  Created by John Rikard Nilsen on 13.10.20.
//

#import <Localytics/Localytics.h>
#import <Flutter/Flutter.h>

@interface LLAnalyticsRelayer : NSObject<LLAnalyticsDelegate>
@property (strong, nonatomic) FlutterMethodChannel *channel;
- (instancetype)initWithChannel:(FlutterMethodChannel*)channel;
@end
