//
//  LocalyticsTranslator.h
//  localytics
//
//  Created by John Rikard Nilsen on 21.10.20.
//

#import <Foundation/Foundation.h>
@import Localytics;

NS_ASSUME_NONNULL_BEGIN

@interface LLLocalyticsTranslator : NSObject
+(NSDictionary *) toRegionMap:(LLRegion *)region;
+(NSArray<NSDictionary *> *) toListOfRegionMap:(NSArray<LLRegion *> *)regions;
+(LLRegionEvent) toRegionEvent:(NSString *)raw;
+(NSString *) toRegionEventString:(LLRegionEvent)raw;
+(NSDictionary *) toLocationMap:(CLLocation *)location;
+(NSDictionary *)toCampaignDictionary:(LLCampaignBase *)campaign;
+(NSDictionary *)toInAppCampaignDictionary:(LLInAppCampaign *)campaign;
+(NSDictionary *)toPlacesCampaignDictionary:(LLPlacesCampaign *)campaign;
+(NSDictionary *)toInboxCampaignDictionary:(LLInboxCampaign *)campaign;
@end

NS_ASSUME_NONNULL_END
