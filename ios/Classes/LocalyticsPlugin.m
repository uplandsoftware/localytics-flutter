#import "LocalyticsPlugin.h"
#import "LLAnalyticsRelayer.h"
#import "LLCallToActionRelayer.h"
#import "LLLocationRelayer.h"
#import "LLMessagingRelayer.h"
#import "LLLocalyticsTranslator.h"
@import Localytics;

@interface LocalyticsPlugin ()
@property (nonatomic, strong) NSMutableDictionary<NSNumber *, LLInboxCampaign *> *inboxCampaignCache;
@property (nonatomic, strong) NSMutableDictionary<NSNumber *, LLInAppCampaign *> *inAppCampaignCache;
@property (nonatomic, strong) NSMutableDictionary<NSNumber *, LLPlacesCampaign *> *placesCache;
@property (nonatomic, strong) LLAnalyticsRelayer *analyticsListener;
@property (nonatomic, strong) LLLocationRelayer *locationRelayer;
@property (nonatomic, strong) LLMessagingRelayer *messagingRelayer;
@property (nonatomic, strong) LLCallToActionRelayer *callToActionRelayer;
@property (nonatomic) dispatch_queue_t inboxCacheSerialQueue;
@end

@implementation LocalyticsPlugin

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  FlutterMethodChannel* channel = [FlutterMethodChannel
      methodChannelWithName:@"localytics"
            binaryMessenger:[registrar messenger]];
  LocalyticsPlugin* instance = [[LocalyticsPlugin alloc] init];
  instance.channel = channel;
  [registrar addMethodCallDelegate:instance channel:channel];
}

- (instancetype)init {
    _inboxCampaignCache = [NSMutableDictionary new];
    _inAppCampaignCache = [NSMutableDictionary new];
    _placesCache = [NSMutableDictionary new];
    _inboxCacheSerialQueue = dispatch_queue_create("com.localytics.LLShouldDeepLink", DISPATCH_QUEUE_SERIAL);
    return self;
}

- (id)nilOrValue:(id)argument {
    if ([argument isKindOfClass:[NSNull class]]) {
        return nil;
    } else {
        return argument;
    }
}

- (id)convertToCustomer:(id)argument {
    return [LLCustomer customerWithBlock:^(LLCustomerBuilder * _Nonnull builder) {
        builder.customerId = argument[@"customerId"];
        builder.firstName = argument[@"firstName"];
        builder.lastName = argument[@"lastName"];
        builder.fullName = argument[@"fullName"];
        builder.emailAddress = argument[@"emailAddress"];
    }];
}

- (LLProfileScope) convertToProfileScope:(id)argument {
    if ([argument isEqualToString:@"org"]) {
        return LLProfileScopeOrganization;
    } else {
        return LLProfileScopeApplication;
    }
}

- (NSDate*) convertToDate:(id)argument {
    NSString *dateString = [argument substringWithRange:NSMakeRange(0, 10)];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    return [dateFormatter dateFromString:dateString];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    NSLog(@"method call: %@ %@", call.method, call.arguments);
    if ([@"getPlatformVersion" isEqualToString:call.method]) {
        result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
    } else {
        id platforms = call.arguments[@"platform"];
        if (platforms != nil && ![platforms containsObject:@"iOS"]) {
            return;
        }
        id arguments = call.arguments[@"values"];
        id types = call.arguments[@"types"];
        NSUInteger numArguments = [arguments count];
        id first, second, third, fourth, fifth;
        if (numArguments > 0) {
            first = [self nilOrValue: arguments[0]];
            if (numArguments > 1) {
                second = [self nilOrValue: arguments[1]];
                if (numArguments > 2) {
                    third = [self nilOrValue:arguments[2]];
                    if (numArguments > 3) {
                        fourth = [self nilOrValue:arguments[3]];
                        if (numArguments > 4) {
                            fifth = [self nilOrValue:arguments[4]];
                        }
                    }
                }
            }
        }
        if ([call.method isEqualToString:@"openSession"]) {
            [Localytics openSession];
        }
        else if ([call.method isEqualToString:@"closeSession"]) {
            [Localytics closeSession];
        }
        else if ([call.method isEqualToString:@"upload"]) {
            [Localytics upload];
        }
        else if ([call.method isEqualToString:@"pauseDataUploading"]) {
            [Localytics pauseDataUploading:first];
        }
        else if ([call.method isEqualToString:@"setOptedOut"]) {
            [Localytics setOptedOut:first];
        }
        else if ([call.method isEqualToString:@"setPrivacyOptedOut"]) {
            [Localytics setPrivacyOptedOut:first];
        }
        else if ([call.method isEqualToString:@"isOptedOut"]) {
            result([NSNumber numberWithBool:Localytics.isOptedOut]);
        }
        else if ([call.method isEqualToString:@"isPrivacyOptedOut"]) {
            result([NSNumber numberWithBool: Localytics.isPrivacyOptedOut]);
        }
        else if ([call.method isEqualToString:@"tagEvent"] && [arguments count] == 1) {
            [Localytics tagEvent:first];
        }
        else if ([call.method isEqualToString:@"tagEvent"] && [arguments count] == 2) {
            [Localytics tagEvent:first attributes: second];
        }
        else if ([call.method isEqualToString:@"tagEvent"] && [arguments count] == 3) {
            [Localytics tagEvent:first attributes:second customerValueIncrease:third];
        }
        else if ([call.method isEqualToString:@"tagPurchased"]) {
            [Localytics tagPurchased:first itemId:second itemType:third itemPrice:fourth attributes:fifth];
        }
        else if ([call.method isEqualToString:@"tagAddedToCart"]) {
            [Localytics tagAddedToCart:first itemId:second itemType:third itemPrice:fourth attributes:fifth];
        }
        else if ([call.method isEqualToString:@"tagStartedCheckout"]) {
            [Localytics tagStartedCheckout:first itemCount:second attributes:third];
        }
        else if ([call.method isEqualToString:@"tagCompletedCheckout"]) {
            [Localytics tagCompletedCheckout:first itemCount:second attributes:third];
        }
        else if ([call.method isEqualToString:@"tagContentViewed"]) {
            [Localytics tagContentViewed:first contentId:second contentType:third attributes:fourth];
        }
        else if ([call.method isEqualToString:@"tagSearched"]) {
            [Localytics tagSearched:first contentType:second resultCount:third attributes:fourth];
        }
        else if ([call.method isEqualToString:@"tagShared"]) {
            [Localytics tagShared:first contentId:second contentType:third methodName:fourth attributes:fifth];
        }
        else if ([call.method isEqualToString:@"tagContentRated"]) {
            [Localytics tagContentRated:first contentId:second contentType:third rating:fourth attributes:fifth];
        }
        else if ([call.method isEqualToString:@"tagCustomerRegistered"]) {
            [Localytics tagCustomerRegistered:[self convertToCustomer:first] methodName:second attributes:third];
        }
        else if ([call.method isEqualToString:@"tagCustomerLoggedIn"]) {
            [Localytics tagCustomerLoggedIn:[self convertToCustomer:first] methodName:second attributes:third];
        }
        else if ([call.method isEqualToString:@"tagCustomerLoggedOut"]) {
            [Localytics tagCustomerLoggedOut:first];
        }
        else if ([call.method isEqualToString:@"tagInvited"]) {
            [Localytics tagInvited:first attributes:second];
        }
        else if ([call.method isEqualToString:@"tagScreen"]) {
            [Localytics tagScreen:first];
        }
        else if ([call.method isEqualToString:@"setCustomDimension"]) {
            [Localytics setValue:second forCustomDimension:[first integerValue]];
        }
        else if ([call.method isEqualToString:@"getCustomDimension"]) {
            result([Localytics valueForCustomDimension:[first integerValue]]);
        }
        else if ([call.method isEqualToString:@"setProfileAttribute"]) {
            id normalizedSecond;
            if ([types[1] isEqualToString:@"Date"]) {
                normalizedSecond = [self convertToDate:second];
            } else if ([types[1] isEqualToString:@"Date[]"]) {
                NSMutableArray *dates = [NSMutableArray new];
                for (NSString *dateString in second) {
                    [dates addObject:[self convertToDate:dateString]];
                }
                normalizedSecond = dates;
            } else {
                normalizedSecond = second;
            }
            [Localytics setValue:normalizedSecond forProfileAttribute:first withScope:[self convertToProfileScope:third]];
        }
        else if ([call.method isEqualToString:@"addProfileAttributesToSet"]) {
            id normalizedSecond;
            if ([types[1] isEqualToString:@"Date"]) {
                normalizedSecond = [self convertToDate:second];
            } else if ([types[1] isEqualToString:@"Date[]"]) {
                NSMutableArray *dates = [NSMutableArray new];
                for (NSString *dateString in second) {
                    [dates addObject:[self convertToDate:dateString]];
                }
                normalizedSecond = dates;
            } else {
                normalizedSecond = second;
            }
            [Localytics addValues:normalizedSecond toSetForProfileAttribute:first withScope:[self convertToProfileScope:third]];
        }
        else if ([call.method isEqualToString:@"removeProfileAttributesFromSet"]) {
            id normalizedSecond;
            if ([types[1] isEqualToString:@"Date"]) {
                normalizedSecond = [self convertToDate:second];
            } else if ([types[1] isEqualToString:@"Date[]"]) {
                NSMutableArray *dates = [NSMutableArray new];
                for (NSString *dateString in second) {
                    [dates addObject:[self convertToDate:dateString]];
                }
                normalizedSecond = dates;
            } else {
                normalizedSecond = second;
            }
            [Localytics removeValues:normalizedSecond fromSetForProfileAttribute:first withScope:[self convertToProfileScope:third]];
        }
        else if ([call.method isEqualToString:@"incrementProfileAttribute"]) {
            [Localytics incrementValueBy:(NSInteger) second forProfileAttribute:first withScope:[self convertToProfileScope:third]];
        }
        else if ([call.method isEqualToString:@"decrementProfileAttribute"]) {
            [Localytics decrementValueBy:(NSInteger) second forProfileAttribute:first withScope:[self convertToProfileScope:third]];
        }
        else if ([call.method isEqualToString:@"deleteProfileAttribute"]) {
            [Localytics deleteProfileAttribute:first withScope:[self convertToProfileScope:second]];
        }
        else if ([call.method isEqualToString:@"setCustomerEmail"]) {
            [Localytics setCustomerEmail:first];
        }
        else if ([call.method isEqualToString:@"setCustomerFirstName"]) {
            [Localytics setCustomerFirstName:first];
        }
        else if ([call.method isEqualToString:@"setCustomerLastName"]) {
            [Localytics setCustomerLastName:first];
        }
        else if ([call.method isEqualToString:@"setCustomerFullName"]) {
            [Localytics setCustomerFullName:first];
        }
        else if ([call.method isEqualToString:@"triggerInAppMessage"] && [arguments count] == 1) {
            [Localytics triggerInAppMessage:first];
        }
        else if ([call.method isEqualToString:@"triggerInAppMessage"] && [arguments count] == 2) {
            [Localytics triggerInAppMessage:first withAttributes:second];
        }
        else if ([call.method isEqualToString:@"triggerInAppMessagesForSessionStart"]) {
            [Localytics triggerInAppMessagesForSessionStart];
        }
        else if ([call.method isEqualToString:@"dismissCurrentInAppMessage"]) {
            [Localytics dismissCurrentInAppMessage];
        }
        else if ([call.method isEqualToString:@"setPushRegistrationId"]) {
            [Localytics setPushToken:first];
        }
        else if ([call.method isEqualToString:@"getPushRegistrationId"]) {
            result(Localytics.pushToken);
        }
        else if ([call.method isEqualToString:@"getGeofencesToMonitor"]) {
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([first doubleValue], [second doubleValue]);
            NSArray<LLRegion *> *regions = [Localytics geofencesToMonitor:coordinate];
            result([LLLocalyticsTranslator toListOfRegionMap:regions]);
        }
        else if ([call.method isEqualToString:@"triggerRegion"]) {
            CLRegion *r = [[CLCircularRegion alloc] initWithCenter:CLLocationCoordinate2DMake(0.0, 0.0) radius:1 identifier:first[@"uniqueId"]];
            CLLocation *l = [[CLLocation alloc] initWithLatitude:[third[@"latitude"] doubleValue] longitude:[third[@"longitude"] doubleValue]];
            [Localytics triggerRegion:r withEvent:[LLLocalyticsTranslator toRegionEvent:second] atLocation:l];
        }
        else if ([call.method isEqualToString:@"triggerRegions"]) {
            NSMutableArray *regions = [NSMutableArray new];
            for (NSDictionary *regionMap in first) {
                CLRegion *r = [[CLCircularRegion alloc] initWithCenter:CLLocationCoordinate2DMake(0.0, 0.0) radius:1 identifier:regionMap[@"uniqueId"]];
                [regions addObject:r];
            }
            CLLocation *l = [[CLLocation alloc] initWithLatitude:[third[@"latitude"] doubleValue] longitude:[third[@"longitude"] doubleValue]];
            [Localytics triggerRegions:regions withEvent:[LLLocalyticsTranslator toRegionEvent:second] atLocation:l];
        }
        else if ([call.method isEqualToString:@"setCustomerId"]) {
            [Localytics setCustomerId:first];
        }
        else if ([call.method isEqualToString:@"setIdentifier"]) {
            [Localytics setValue:second forIdentifier:first];
        }
        else if ([call.method isEqualToString:@"setCustomerIdWithPrivacyOptedOut"]) {
            // ignore
        }
        else if ([call.method isEqualToString:@"getCustomerId"]) {
            result(Localytics.customerId);
        }
        else if ([call.method isEqualToString:@"getIdentifier"]) {
            result([Localytics valueForIdentifier:first]);
        }
        else if ([call.method isEqualToString:@"setLocation"]) {
            CLLocationCoordinate2D coordinate;
            coordinate.longitude = [first[@"longitude"] doubleValue];
            coordinate.latitude = [first[@"latitude"] doubleValue];
            [Localytics setLocation:coordinate];
        }
        else if ([call.method isEqualToString:@"setLoggingEnabled"]) {
            [Localytics setLoggingEnabled:first];
        }
        else if ([call.method isEqualToString:@"isLoggingEnabled"]) {
            result([NSNumber numberWithBool:Localytics.isLoggingEnabled]);
        }
        else if ([call.method isEqualToString:@"getInstallId"]) {
            result(Localytics.installId);
        }
        else if ([call.method isEqualToString:@"getLocalAuthenticationToken"]) {
            // ignore
        }
        else if ([call.method isEqualToString:@"getAppKey"]) {
            result(Localytics.appKey);
        }
        else if ([call.method isEqualToString:@"getAllInboxCampaigns"]) {
            NSArray<LLInboxCampaign *> *campaigns = [Localytics allInboxCampaigns];
            [self replaceInboxCache:campaigns];
            NSMutableArray<NSDictionary *> *rawCampaigns = [NSMutableArray new];
            for(LLInboxCampaign *campaign in campaigns) {
                [rawCampaigns addObject:[LLLocalyticsTranslator toInboxCampaignDictionary:campaign]];
            }
            result(rawCampaigns);
        }
        else if ([call.method isEqualToString:@"getDisplayableInboxCampaigns"]) {
            NSArray<LLInboxCampaign *> *campaigns = [Localytics displayableInboxCampaigns];
            [self updateInboxCache:campaigns];
            NSMutableArray<NSDictionary *> *rawCampaigns = [NSMutableArray new];
            for(LLInboxCampaign *campaign in campaigns) {
                [rawCampaigns addObject:[LLLocalyticsTranslator toInboxCampaignDictionary:campaign]];
            }
            result(rawCampaigns);
        }
        else if ([call.method isEqualToString:@"setInboxCampaignRead"]) {
            dispatch_sync(_inboxCacheSerialQueue, ^{
                LLInboxCampaign* campaign = _inboxCampaignCache[first];
                if (campaign==nil) {
                    NSLog(@"No campaign found for id :%ld", (long)first);
                }
                [Localytics setInboxCampaign:campaign asRead:second];
            });
        }
        else if ([call.method isEqualToString:@"getInboxCampaignsUnreadCount"]) {
            result([NSNumber numberWithLong:[Localytics inboxCampaignsUnreadCount]]);
        }
        else if ([call.method isEqualToString:@"deleteInboxCampaign"]) {
            [Localytics deleteInboxCampaign:_inboxCampaignCache[first]];
        }
        else if ([call.method isEqualToString:@"inboxListItemTapped"]) {
            [Localytics inboxListItemTapped:_inboxCampaignCache[first]];
        }
        else if ([call.method isEqualToString:@"setAnalyticsListener"]) {
            if (!first) {
                [Localytics setAnalyticsDelegate:nil];
                return;
            }
            _analyticsListener = [[LLAnalyticsRelayer alloc] initWithChannel:_channel];
            [Localytics setAnalyticsDelegate:_analyticsListener];
        }
        else if ([call.method isEqualToString:@"setLocationListener"]) {
            if (!first) {
                [Localytics setLocationDelegate:nil];
                return;
            }
            _locationRelayer = [[LLLocationRelayer alloc] initWithChannel:_channel];
            [Localytics setLocationDelegate:_locationRelayer];
        }
        else if ([call.method isEqualToString:@"setMessagingListener"]) {
            if (!first) {
                [Localytics setMessagingDelegate:nil];
                return;
            }
            _messagingRelayer = [[LLMessagingRelayer alloc] initWithChannel:_channel inAppCache:_inAppCampaignCache placesCache:_placesCache];
            NSDictionary *prunedInAppConfig = [self pruneNullValues:second];
            NSDictionary *prunedPlacesConfig = [self pruneNullValues:fourth];
            _messagingRelayer.inAppConfig = prunedInAppConfig;
            _messagingRelayer.placesConfig = prunedPlacesConfig;
            [Localytics setMessagingDelegate:_messagingRelayer];
        }
        else if ([call.method isEqualToString:@"tagInAppImpression"]) {
            LLInAppCampaign *campaign = _inAppCampaignCache[first];
            if([types[1] isEqualToString:@"String"]) {
                [Localytics tagImpressionForInAppCampaign:campaign withCustomAction:second];
            } else {
                LLImpressionType impressionType = [second isEqualToString:@"click"] ? LLImpressionTypeClick : LLImpressionTypeDismiss;
                [Localytics tagImpressionForInAppCampaign:campaign withType:impressionType];
            }
        }
        else if ([call.method isEqualToString:@"tagInboxImpression"]) {
            LLInboxCampaign *campaign = _inboxCampaignCache[first];
            if([types[1] isEqualToString:@"String"]) {
                [Localytics tagImpressionForInboxCampaign:campaign withCustomAction:second];
            } else {
                LLImpressionType impressionType = [second isEqualToString:@"click"] ? LLImpressionTypeClick : LLImpressionTypeDismiss;
                [Localytics tagImpressionForInboxCampaign:campaign withType:impressionType];
            }
        }
        else if ([call.method isEqualToString:@"tagPushToInboxImpression"]) {
            LLInboxCampaign *campaign = _inboxCampaignCache[first];
            // TODO: figure out why these are different on android and iOS
            [Localytics tagImpressionForPushToInboxCampaign:campaign success:YES];
        }
        else if ([call.method isEqualToString:@"tagPlacesPushReceived"]) {
            LLPlacesCampaign *campaign = _placesCache[first];
            [Localytics tagPlacesPushReceived:campaign];
        }
        else if ([call.method isEqualToString:@"tagPlacesPushOpened"]) {
            LLPlacesCampaign *campaign = _placesCache[first];
            if (second == nil) {
                [Localytics tagPlacesPushOpened:campaign];
            } else {
                [Localytics tagPlacesPushOpened:campaign withActionIdentifier:second];
            }
        }
        else if ([call.method isEqualToString:@"triggerPlacesNotification"]) {
            if (second == nil) {
                LLPlacesCampaign *campaign = _placesCache[first];
                [Localytics triggerPlacesNotificationForCampaign:campaign];
            } else {
                [Localytics triggerPlacesNotificationForCampaignId:(NSInteger) first regionIdentifier:second];
            }
        }
        else if ([call.method isEqualToString:@"requestAdvertisingIdentifierPrompt"]) {
            result([NSNumber numberWithBool:[Localytics requestAdvertisingIdentifierPrompt]]);
        }
        else if ([call.method isEqualToString:@"advertisingIdentifierStatus"]) {
            result([NSNumber numberWithInteger:[Localytics advertisingIdentifierStatus]]);
        }
        else if ([call.method isEqualToString:@"setCallToActionListener"]) {
            if (!first) {
                [Localytics setCallToActionDelegate:nil];
                return;
            }
            _callToActionRelayer = [[LLCallToActionRelayer alloc] initWithChannel:_channel];
            [Localytics setCallToActionDelegate:_callToActionRelayer];
        }
        else {
            NSLog(@"%@", [@"unable to execute method " stringByAppendingString:[call method]]);
        }

        result(nil);
    }
}

- (NSDictionary *)pruneNullValues:(NSDictionary *) input {
    NSMutableDictionary *pruned = [NSMutableDictionary dictionary];
    if (input != nil) {
        for (NSString * key in [input allKeys])
        {
            if (![[input objectForKey:key] isKindOfClass:[NSNull class]])
                [pruned setObject:[input objectForKey:key] forKey:key];
        }
    }
    return pruned;
}


- (void)updateInboxCache:(nonnull NSArray<LLInboxCampaign *> *)  campaigns {
    dispatch_sync(_inboxCacheSerialQueue, ^{
        // Cache campaigns - Dont clear out here. This is not all campaigns
        for (LLInboxCampaign *campaign in campaigns) {
            [self.inboxCampaignCache setObject:campaign forKey:@(campaign.campaignId)];
        }
    });
}

- (void)replaceInboxCache:(nonnull NSArray<LLInboxCampaign *> *) campaigns {
    // Do an atomic update of campaign cache to avoid issues when accessing from multiple queues.
    NSMutableDictionary* newCache = [NSMutableDictionary new];
    for (LLInboxCampaign *campaign in campaigns) {
        [newCache setObject:campaign forKey:@(campaign.campaignId)];
    }
    dispatch_sync(_inboxCacheSerialQueue, ^{
        self.inboxCampaignCache = newCache;
    });
}

@end
