//
//  LLMessagingRelayer.m
//  localytics
//
//  Created by John Rikard Nilsen on 27.10.20.
//

#import "LLMessagingRelayer.h"
#import "LLLocalyticsTranslator.h"
@import UserNotifications;

@interface LLMessagingRelayer ()
@property (nonatomic, strong) NSMutableDictionary<NSNumber *, LLInAppCampaign *> *inAppCache;
@property (nonatomic, strong) NSMutableDictionary<NSNumber *,LLPlacesCampaign *> *placesCache;
@end

@implementation LLMessagingRelayer

- (instancetype)initWithChannel:(FlutterMethodChannel *)channel
                     inAppCache:(NSMutableDictionary<NSNumber *,LLInAppCampaign *> *)inAppCache
                    placesCache:(NSMutableDictionary<NSNumber *,LLPlacesCampaign *> *)placesCache{
    self.channel = channel;
    self.inAppCache = inAppCache;
    self.placesCache = placesCache;
    return self;
}

- (BOOL)localyticsShouldShowInAppMessage:(nonnull LLInAppCampaign *)campaign {
    // Cache campaign
    [self.inAppCache setObject:campaign forKey:@(campaign.campaignId)];

    BOOL shouldShow = YES;
    if (self.inAppConfig) {

        // Global Suppression
        if (self.inAppConfig[@"shouldShow"]) {
            shouldShow = [self.inAppConfig[@"shouldShow"] boolValue];
        }

        // DIY In-App. This callback will suppress the in-app and emit an event
        // for manually handling
        if (self.inAppConfig[@"diy"] && [self.inAppConfig[@"diy"] boolValue]) {
            NSDictionary *body = [LLLocalyticsTranslator toInAppCampaignDictionary:campaign];
            [_channel invokeMethod:@"localyticsDiyInAppMessage" arguments:body];
            return NO;
        }
    }

    NSDictionary *rawCampaign = [LLLocalyticsTranslator toInAppCampaignDictionary:campaign];
    NSMutableDictionary *body =  [rawCampaign mutableCopy];
    [body setValue:[NSNumber numberWithBool:shouldShow] forKey:@"shouldShow"];
    [_channel invokeMethod:@"localyticsShouldShowInAppMessage" arguments:body];

    return shouldShow;
}

- (BOOL)localyticsShouldDelaySessionStartInAppMessages {
    BOOL shouldDelay = NO;
    if (self.inAppConfig && self.inAppConfig[@"delaySessionStart"]) {
        shouldDelay = [self.inAppConfig[@"delaySessionStart"] boolValue];
    }

    NSDictionary *body = @{@"shouldDelay": @(shouldDelay)};
    [_channel invokeMethod:@"localyticsShouldDelaySessionStartInAppMessages" arguments:body];

    return shouldDelay;
}

- (nonnull LLInAppConfiguration *)localyticsWillDisplayInAppMessage:(nonnull LLInAppCampaign *)campaign withConfiguration:(nonnull LLInAppConfiguration *)configuration {
    if (self.inAppConfig) {
        if (_inAppConfig[@"dismissButtonLocation"]) {
            if ([_inAppConfig[@"dismissButtonLocation"]  isEqualToString:@"left"]) {
                configuration.dismissButtonLocation = LLInAppMessageDismissButtonLocationLeft;
            } else {
                configuration.dismissButtonLocation = LLInAppMessageDismissButtonLocationRight;
            }
        }
        if (_inAppConfig[@"dismissButtonHidden"]) {
            configuration.dismissButtonHidden = [_inAppConfig[@"dismissButtonHidden"] boolValue];
        }
        if (_inAppConfig[@"dismissButtonImageName"]) {
            [configuration setDismissButtonImageWithName:_inAppConfig[@"dismissButtonImageName"]];
        }
        if (_inAppConfig[@"aspectRatio"]) {
            configuration.aspectRatio = [_inAppConfig[@"aspectRatio"] floatValue];
        }
        if (_inAppConfig[@"offset"]) {
            configuration.offset = [_inAppConfig[@"offset"] floatValue];
        }
        if (_inAppConfig[@"backgroundAlpha"]) {
            configuration.backgroundAlpha = [_inAppConfig[@"backgroundAlpha"] floatValue];
        }
        if (_inAppConfig[@"autoHideHomeScreenIndicator"]) {
            configuration.autoHideHomeScreenIndicator = [_inAppConfig[@"autoHideHomeScreenIndicator"] boolValue];
        }
        if (_inAppConfig[@"notchFullScreen"]) {
            configuration.notchFullScreen = [_inAppConfig[@"notchFullScreen"] boolValue];
        }
        if (_inAppConfig[@"videoConversionPercentage"]) {
            configuration.videoConversionPercentage = [_inAppConfig[@"videoConversionPercentage"] floatValue];
        }
    }

    NSDictionary *body = [LLLocalyticsTranslator toInAppCampaignDictionary:campaign];
    [_channel invokeMethod:@"localyticsWillDisplayInAppMessage" arguments:body];

    return configuration;
}

- (void)localyticsDidDisplayInAppMessage {
    [_channel invokeMethod:@"localyticsDidDisplayInAppMessage" arguments:nil];
}

- (void)localyticsWillDismissInAppMessage {
    [_channel invokeMethod:@"localyticsWillDismissInAppMessage" arguments:nil];
}

- (void)localyticsDidDismissInAppMessage {
    [_channel invokeMethod:@"localyticsDidDismissInAppMessage" arguments:nil];
}

- (void)localyticsWillDisplayInboxDetailViewController {
    [_channel invokeMethod:@"localyticsWillDisplayInboxDetailViewController" arguments:nil];
}

- (void)localyticsDidDisplayInboxDetailViewController {
    [_channel invokeMethod:@"localyticsDidDisplayInboxDetailViewController" arguments:nil];
}

- (void)localyticsWillDismissInboxDetailViewController {
    [_channel invokeMethod:@"localyticsWillDismissInboxDetailViewController" arguments:nil];
}

- (void)localyticsDidDismissInboxDetailViewController {
    [_channel invokeMethod:@"localyticsDidDismissInboxDetailViewController" arguments:nil];
}

- (BOOL)localyticsShouldDisplayPlacesCampaign:(nonnull LLPlacesCampaign *)campaign {
    // Cache campaign
    [_placesCache setObject:campaign forKey:@(campaign.campaignId)];

    BOOL shouldShow = YES;
    if (self.placesConfig) {
        // Global Suppression
        if (self.placesConfig[@"shouldShow"]) {
            shouldShow = [self.placesConfig[@"shouldShow"] boolValue];
        }

        // DIY Places. This callback will suppress the Places push and emit an event
        // for manually handling
        if (self.placesConfig[@"diy"] && [self.placesConfig[@"diy"] boolValue]) {
            NSDictionary *body = [LLLocalyticsTranslator toPlacesCampaignDictionary:campaign];
            [_channel invokeMethod:@"localyticsDiyPlacesPushNotification" arguments:body];

            return NO;
        }
    }

    NSDictionary *body = [LLLocalyticsTranslator toPlacesCampaignDictionary:campaign];
    [_channel invokeMethod:@"localyticsShouldShowPlacesPushNotification" arguments:body];

    return shouldShow;
}

- (nonnull UILocalNotification *)localyticsWillDisplayNotification:(nonnull UILocalNotification *)notification forPlacesCampaign:(nonnull LLPlacesCampaign *)campaign {
    if (self.placesConfig) {
        if (self.placesConfig[@"alertAction"]) {
            notification.alertAction = self.placesConfig[@"alertAction"];
        }
        if (self.placesConfig[@"alertTitle"]) {
            if (@available(iOS 8.2, *)) {
                notification.alertTitle = self.placesConfig[@"alertTitle"];
            } else {
                // Fallback on earlier versions
            }
        }
        if (self.placesConfig[@"hasAction"]) {
            notification.hasAction = [self.placesConfig[@"hasAction"] boolValue];
        }
        if (self.placesConfig[@"alertLaunchImage"]) {
            notification.alertLaunchImage = self.placesConfig[@"alertLaunchImage"];
        }
        if (self.placesConfig[@"category"]) {
            notification.category = self.placesConfig[@"category"];
        }
        if (self.placesConfig[@"applicationIconBadgeNumber"]) {
            notification.applicationIconBadgeNumber = [self.placesConfig[@"applicationIconBadgeNumber"] integerValue];
        }
        if (self.placesConfig[@"soundName"]) {
            notification.soundName = self.placesConfig[@"soundName"];
        }
    }
    
    NSDictionary *body = [LLLocalyticsTranslator toPlacesCampaignDictionary:campaign];
    [_channel invokeMethod:@"localyticsWillShowPlacesPushNotification" arguments:body];
    
    return notification;
}

- (nonnull UNMutableNotificationContent *)localyticsWillDisplayNotificationContent:(nonnull UNMutableNotificationContent *)notification forPlacesCampaign:(nonnull LLPlacesCampaign *)campaign  API_AVAILABLE(ios(10.0)){
    if (self.placesConfig) {
        if (self.placesConfig[@"title"]) {
            notification.title = self.placesConfig[@"title"];
        }
        if (self.placesConfig[@"subtitle"]) {
            notification.subtitle = self.placesConfig[@"subtitle"];
        }
        if (self.placesConfig[@"badge"]) {
            notification.badge = @([self.placesConfig[@"badge"] integerValue]);
        }
        if (self.placesConfig[@"sound"]) {
            if (@available(iOS 10.0, *)) {
                notification.sound = [UNNotificationSound soundNamed:self.placesConfig[@"sound"]];
            } else {
                // Fallback on earlier versions
            }
        }
        if (self.placesConfig[@"launchImageName"]) {
            notification.launchImageName = self.placesConfig[@"launchImageName"];
        }
    }
    
    NSDictionary *body = [LLLocalyticsTranslator toPlacesCampaignDictionary:campaign];
    [_channel invokeMethod:@"localyticsWillShowPlacesPushNotification" arguments:body];

    return notification;
}

@end
