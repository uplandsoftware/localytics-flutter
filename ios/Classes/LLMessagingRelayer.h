//
//  LLMessagingRelayer.h
//  localytics
//
//  Created by John Rikard Nilsen on 27.10.20.
//

#import <Localytics/Localytics.h>
#import <Flutter/Flutter.h>

NS_ASSUME_NONNULL_BEGIN

@interface LLMessagingRelayer : NSObject<LLMessagingDelegate>
@property (strong, nonatomic) FlutterMethodChannel *channel;
@property (nonatomic, strong) NSDictionary *inAppConfig;
@property (nonatomic, strong) NSDictionary *placesConfig;
- (instancetype)initWithChannel:(FlutterMethodChannel *)channel
                     inAppCache:(NSMutableDictionary<NSNumber *,LLInAppCampaign *> *)inAppCache
                    placesCache:(NSMutableDictionary<NSNumber *,LLPlacesCampaign *> *)placesCache;
@end

NS_ASSUME_NONNULL_END
