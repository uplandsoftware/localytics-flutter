//
//  LocationRelayer.h
//  Pods
//
//  Created by John Rikard Nilsen on 19.10.20.
//

#import <Localytics/Localytics.h>
#import <Flutter/Flutter.h>

@interface LLLocationRelayer : NSObject<LLLocationDelegate>
@property (strong, nonatomic) FlutterMethodChannel *channel;
- (instancetype)initWithChannel:(FlutterMethodChannel*)channel;
@end
