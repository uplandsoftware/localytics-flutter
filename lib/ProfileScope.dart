enum ProfileScope { organization, application }

extension ProfileScopeExtension on ProfileScope {
  String rawValue() {
    switch (this) {
      case ProfileScope.application:
        {
          return "app";
        }
      case ProfileScope.organization:
        {
          return "org";
        }
    }
  }
}
