class Location {
  double latitude;
  double longitude;
  double altitude;
  double time;
  // android only
  double accuracy;
  //iOS only
  double horizontalAccuracy;
  //iOS only
  double verticalAccuracy;

  Location(double latitude, double longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{'latitude': latitude, 'longitude': longitude};
  }

  static Location fromMap(Map<dynamic, dynamic> map) {
    Location l = new Location(map['latitude'], map['longitude']);
    l.altitude = map['altitude'];
    l.time = map['time'];
    l.accuracy = map['accuracy'];
    l.horizontalAccuracy = map['horizontalAccuracy'];
    l.verticalAccuracy = map['verticalAccuracy'];
    return l;
  }
}
