class AnalyticsListener {
  void localyticsSessionWillOpen(bool isFirst, bool isUpgrade, bool isResume) {}
  void localyticsSessionDidOpen(bool isFirst, bool isUpgrade, bool isResume) {}
  void localyticsSessionWillClose() {}
  void localyticsDidTagEvent(String eventName, Map<String, String> attributes,
      int customerValueIncrease) {}
}
