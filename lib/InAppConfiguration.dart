class InAppConfiguration {
  bool shouldShow;
  bool diy;
  double aspectRatio;
  double backgroundAlpha;
  int bannerOffsetDps;
  String dismissButtonLocation;
  bool dismissButtonHidden;
  double videoConversionPercentage;
  bool delaySessionStart;

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      "shouldShow": shouldShow,
      "diy": diy,
      "aspectRatio": aspectRatio,
      "backgroundAlpha": backgroundAlpha,
      "bannerOffsetDps": bannerOffsetDps,
      "dismissButtonLocation": dismissButtonLocation,
      "dismissButtonHidden": dismissButtonHidden,
      "videoConversionPercentage": videoConversionPercentage,
      "delaySessionStart": delaySessionStart
    };
  }
}
