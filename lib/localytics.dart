import 'dart:async';

import 'package:flutter/services.dart';

import 'AdIdStatus.dart';
import 'AnalyticsListener.dart';
import 'CallToActionListener.dart';
import 'Campaign.dart';
import 'CircularRegion.dart';
import 'Customer.dart';
import 'ImpressionType.dart';
import 'InAppCampaign.dart';
import 'InAppConfiguration.dart';
import 'InboxCampaign.dart';
import 'Location.dart';
import 'LocationListener.dart';
import 'MessagingListener.dart';
import 'PlacesCampaign.dart';
import 'ProfileScope.dart';
import 'PushCampaign.dart';
import 'PushConfiguration.dart';
import 'Region.dart';
import 'RegionEvent.dart';

class LocalyticsBase {
  static const MethodChannel _channel = const MethodChannel('localytics');

  static Map<String, dynamic> _generateArgumentMap(
      List<Object> values, List<String> types,
      {String returnType = "automatic", List<String> platform}) {
    //by default if no platform explicitly set, we set both
    if (platform == null) {
      platform = new List<String>();
      platform.add("android");
      platform.add("iOS");
    }
    return <String, dynamic>{
      'values': values,
      'types': types,
      'returnType': returnType,
      'platform': platform
    };
  }
}

class Localytics {
  static AnalyticsListener _analyticsListener;
  static LocationListener _locationListener;
  static MessagingListener _messagingListener;
  static CallToActionListener _callToActionListener;
  static bool _wasConfigured = false;
  static const MethodChannel _channel = LocalyticsBase._channel;

  static void configure() {
    if (_wasConfigured) {
      return;
    }
    _channel.setMethodCallHandler((call) {
      String methodName = call.method;
      switch (methodName) {
        case "localyticsSessionWillOpen":
          {
            if (_analyticsListener != null) {
              _analyticsListener.localyticsSessionWillOpen(
                  call.arguments[0], call.arguments[1], call.arguments[2]);
            }
            break;
          }
        case "localyticsSessionDidOpen":
          {
            if (_analyticsListener != null) {
              _analyticsListener.localyticsSessionDidOpen(
                  call.arguments[0], call.arguments[1], call.arguments[2]);
            }
            break;
          }
        case "localyticsDidTagEvent":
          {
            if (_analyticsListener != null) {
              String event = call.arguments[0];
              print("Localytics plugin tagging event: $event");
              Map<dynamic, dynamic> rawAttributes = call.arguments[1];
              Map<String, String> attributes;
              if (rawAttributes != null) {
                attributes = rawAttributes.map((rKey, rValue) {
                  String key = rKey;
                  String value = rValue;
                  return MapEntry<String, String>(key, value);
                });
              }
              _analyticsListener.localyticsDidTagEvent(
                  call.arguments[0], attributes, call.arguments[2]);
            }
            break;
          }
        case "localyticsDidUpdateLocation":
          {
            if (_locationListener != null) {
              _locationListener.localyticsDidUpdateLocation(
                  Location.fromMap(call.arguments));
            }
            break;
          }
        case "localyticsDidTriggerRegions":
          {
            if (_locationListener != null) {
              _locationListener.localyticsDidTriggerRegions(
                  CircularRegion.listFromListOfMaps(call.arguments[0]),
                  RegionEventExtension.fromString(call.arguments[1]));
            }
            break;
          }
        case "localyticsDidUpdateMonitoredGeofences":
          {
            if (_locationListener != null) {
              _locationListener.localyticsDidUpdateMonitoredGeofences(
                  CircularRegion.listFromListOfMaps(call.arguments[0]),
                  CircularRegion.listFromListOfMaps(call.arguments[1]));
            }
            break;
          }
        case "localyticsShouldShowInAppMessage":
          {
            if (_messagingListener != null) {
              _messagingListener.localyticsShouldShowInAppMessage(
                  InAppCampaign.fromMap(call.arguments));
            }
            break;
          }
        case "localyticsWillDisplayInAppMessage":
          {
            if (_messagingListener != null) {
              _messagingListener.localyticsWillDisplayInAppMessage(
                  InAppCampaign.fromMap(call.arguments));
            }
            break;
          }
        case "localyticsDidDisplayInAppMessage":
          {
            if (_messagingListener != null) {
              _messagingListener.localyticsDidDisplayInAppMessage();
            }
            break;
          }
        case "localyticsWillDismissInAppMessage":
          {
            if (_messagingListener != null) {
              _messagingListener.localyticsWillDismissInAppMessage();
            }
            break;
          }
        case "localyticsDidDismissInAppMessage":
          {
            if (_messagingListener != null) {
              _messagingListener.localyticsDidDismissInAppMessage();
            }
            break;
          }
        case "localyticsShouldDelaySessionStartInAppMessages":
          {
            if (_messagingListener != null) {
              _messagingListener.localyticsShouldDelaySessionStartInAppMessages(
                  call.arguments);
            }
            break;
          }
        case "localyticsShouldShowPushNotification":
          {
            if (_messagingListener != null) {
              _messagingListener.localyticsShouldShowPushNotification(
                  PushCampaign.fromMap(call.arguments));
            }
            break;
          }
        case "localyticsWillShowPushNotification":
          {
            if (_messagingListener != null) {
              _messagingListener.localyticsWillShowPushNotification(
                  PushCampaign.fromMap(call.arguments));
            }
            break;
          }
        case "localyticsShouldShowPlacesPushNotification":
          {
            if (_messagingListener != null) {
              _messagingListener.localyticsShouldShowPlacesPushNotification(
                  PlacesCampaign.fromMap(call.arguments));
            }
            break;
          }
        case "localyticsWillShowPlacesPushNotification":
          {
            if (_messagingListener != null) {
              _messagingListener.localyticsWillShowPlacesPushNotification(
                  PlacesCampaign.fromMap(call.arguments));
            }
            break;
          }
        case "localyticsShouldDeeplink":
          {
            if (_callToActionListener != null) {
              _callToActionListener.localyticsShouldDeeplink(
                  call.arguments['url'],
                  Campaign.fromMap(call.arguments['campaign']));
            }
            break;
          }
        case "localyticsShouldDeeplinkToSettings":
          {
            if (_callToActionListener != null) {
              _callToActionListener.localyticsShouldDeeplinkToSettings(
                  Campaign.fromMap(call.arguments));
            }
            break;
          }
        case "localyticsDidOptOut":
          {
            if (_callToActionListener != null) {
              _callToActionListener.localyticsDidOptOut(
                  call.arguments['optedOut'],
                  Campaign.fromMap(call.arguments['campaign']));
            }
            break;
          }
        case "localyticsDidPrivacyOptOut":
          {
            if (_callToActionListener != null) {
              _callToActionListener.localyticsDidPrivacyOptOut(
                  call.arguments['optedOut'],
                  Campaign.fromMap(call.arguments['campaign']));
            }
            break;
          }
        case "localyticsShouldPromptForLocationPermissions":
          {
            if (_callToActionListener != null) {
              _callToActionListener
                  .localyticsShouldPromptForLocationPermissions(
                      Campaign.fromMap(call.arguments));
            }
            break;
          }
        case "localyticsShouldPromptForLocationWhenInUsePermissions":
          {
            if (_callToActionListener != null) {
              _callToActionListener
                  .localyticsShouldPromptForLocationWhenInUsePermissions(
                      Campaign.fromMap(call.arguments));
            }
            break;
          }
        case "localyticsShouldPromptForLocationAlwaysPermissions":
          {
            if (_callToActionListener != null) {
              _callToActionListener
                  .localyticsShouldPromptForLocationAlwaysPermissions(
                      Campaign.fromMap(call.arguments));
            }
            break;
          }
        case "localyticsShouldPromptForNotificationPermissions":
          {
            if (_callToActionListener != null) {
              _callToActionListener
                  .localyticsShouldPromptForNotificationPermissions(
                      Campaign.fromMap(call.arguments));
            }
            break;
          }
        default:
          {
            print("received method call $methodName");
          }
      }
    });
    _wasConfigured = true;
  }

  static void openSession() {
    _channel.invokeMethod('openSession');
  }

  static void closeSession() {
    _channel.invokeMethod('closeSession');
  }

  static void upload() {
    _channel.invokeMethod('upload');
  }

  static void pauseDataUploading(bool pause) {
    _channel.invokeMethod(
        'pauseDataUploading', _generateArgumentMap([pause], ['boolean']));
  }

  static void setOptedOut(bool optedOut) {
    _channel.invokeMethod(
        'setOptedOut', _generateArgumentMap([optedOut], ['boolean']));
  }

  static void setPrivacyOptedOut(bool optedOut) {
    _channel.invokeMethod(
        'setPrivacyOptedOut', _generateArgumentMap([optedOut], ['boolean']));
  }

  static Future<bool> isOptedOut() async {
    return await _channel.invokeMethod('isOptedOut');
  }

  static Future<bool> isPrivacyOptedOut() async {
    return await _channel.invokeMethod('isPrivacyOptedOut');
  }

  // event tagging

  static void tagEvent(String event) {
    _channel.invokeMethod(
        'tagEvent', _generateArgumentMap([event], ['String']));
  }

  static void tagEventWithAttributes(
      String event, Map<String, String> attributes) {
    _channel.invokeMethod('tagEvent',
        _generateArgumentMap([event, attributes], ['String', 'Map']));
  }

  static void tagEventWithAttributesAndValueIncrease(
      String event, Map<String, String> attributes, int valueIncrease) {
    _channel.invokeMethod(
        'tagEvent',
        _generateArgumentMap(
            [event, attributes, valueIncrease], ['String', 'Map', 'long']));
  }

  // standard event tagging

  static void tagPurchased(String itemName, String itemId, String itemType,
      int itemPrice, Map<String, String> attributes) {
    _channel.invokeMethod(
        'tagPurchased',
        _generateArgumentMap(
            [itemName, itemId, itemType, itemPrice, attributes],
            ['String', 'String', 'String', 'Long', 'Map']));
  }

  static void tagAddedToCart(String itemName, String itemId, String itemType,
      int itemPrice, Map<String, String> attributes) {
    _channel.invokeMethod(
        'tagAddedToCart',
        _generateArgumentMap(
            [itemName, itemId, itemType, itemPrice, attributes],
            ['String', 'String', 'String', 'Long', 'Map']));
  }

  static void tagStartedCheckout(
      int totalPrice, int itemCount, Map<String, String> attributes) {
    _channel.invokeMethod(
        'tagStartedCheckout',
        _generateArgumentMap(
            [totalPrice, itemCount, attributes], ['Long', 'Long', 'Map']));
  }

  static void tagCompletedCheckout(
      int totalPrice, int itemCount, Map<String, String> attributes) {
    _channel.invokeMethod(
        'tagCompletedCheckout',
        _generateArgumentMap(
            [totalPrice, itemCount, attributes], ['Long', 'Long', 'Map']));
  }

  static void tagContentViewed(String contentName, String contentId,
      String contentType, Map<String, String> attributes) {
    _channel.invokeMethod(
        'tagContentViewed',
        _generateArgumentMap([contentName, contentId, contentType, attributes],
            ['String', 'String', 'String', 'Map']));
  }

  static void tagSearched(String queryText, String contentType, int resultCount,
      Map<String, String> attributes) {
    _channel.invokeMethod(
        'tagSearched',
        _generateArgumentMap([queryText, contentType, resultCount, attributes],
            ['String', 'String', 'Long', 'Map']));
  }

  static void tagShared(String contentName, String contentId,
      String contentType, String methodName, Map<String, String> attributes) {
    _channel.invokeMethod(
        'tagShared',
        _generateArgumentMap(
            [contentName, contentId, contentType, methodName, attributes],
            ['String', 'String', 'String', 'String', 'Map']));
  }

  static void tagContentRated(String contentName, String contentId,
      String contentType, int rating, Map<String, String> attributes) {
    _channel.invokeMethod(
        'tagContentRated',
        _generateArgumentMap(
            [contentName, contentId, contentType, rating, attributes],
            ['String', 'String', 'String', 'Long', 'Map']));
  }

  static void tagCustomerRegistered(
      Customer customer, String methodName, Map<String, String> attributes) {
    _channel.invokeMethod(
        'tagCustomerRegistered',
        _generateArgumentMap([customer.toMap(), methodName, attributes],
            ['Customer', 'String', 'Map']));
  }

  static void tagCustomerLoggedIn(
      Customer customer, String methodName, Map<String, String> attributes) {
    _channel.invokeMethod(
        'tagCustomerLoggedIn',
        _generateArgumentMap([customer.toMap(), methodName, attributes],
            ['Customer', 'String', 'Map']));
  }

  static void tagCustomerLoggedOut(Map<String, String> attributes) {
    _channel.invokeMethod(
        'tagCustomerLoggedOut', _generateArgumentMap([attributes], ['Map']));
  }

  static void tagInvited(String methodName, Map<String, String> attributes) {
    _channel.invokeMethod('tagInvited',
        _generateArgumentMap([methodName, attributes], ['String', 'Map']));
  }

  static void tagInAppImpression(
      InAppCampaign campaign, ImpressionType impressionType) {
    _channel.invokeMethod(
        'tagInAppImpression',
        _generateArgumentMap([campaign.campaignId, impressionType.rawValue()],
            ['InAppCampaign', 'ImpressionType']));
  }

  static void tagInAppImpressionWithCustomAction(
      InAppCampaign campaign, String customAction) {
    _channel.invokeMethod(
        'tagInAppImpression',
        _generateArgumentMap(
            [campaign.campaignId, customAction], ['InAppCampaign', 'String']));
  }

  static void tagInBoxImpression(
      InboxCampaign campaign, ImpressionType impressionType) {
    _channel.invokeMethod(
        'tagInboxImpression',
        _generateArgumentMap([campaign.campaignId, impressionType.rawValue()],
            ['InboxCampaign', 'ImpressionType']));
  }

  static void tagInboxImpressionWithCustomAction(
      InboxCampaign campaign, String customAction) {
    _channel.invokeMethod(
        'tagInboxImpression',
        _generateArgumentMap(
            [campaign.campaignId, customAction], ['InboxCampaign', 'String']));
  }

  static void tagPushToInboxImpression(InboxCampaign campaign) {
    _channel.invokeMethod('tagPushToInboxImpression',
        _generateArgumentMap([campaign.campaignId], ['InboxCampaign']));
  }

  static void tagPlacesPushReceived(PlacesCampaign campaign) {
    _channel.invokeMethod('tagPlacesPushReceived',
        _generateArgumentMap([campaign.campaignId], ['PlacesCampaign']));
  }

  static void tagPlacesPushOpened(PlacesCampaign campaign) {
    _channel.invokeMethod('tagPlacesPushOpened',
        _generateArgumentMap([campaign.campaignId], ['PlacesCampaign']));
  }

  static void tagPlacesPushOpenedWithAction(
      PlacesCampaign campaign, String action) {
    _channel.invokeMethod(
        'tagPlacesPushOpened',
        _generateArgumentMap(
            [campaign.campaignId, action], ['PlacesCampaign', 'String']));
  }

  static void triggerPlacesNotification(PlacesCampaign campaign) {
    _channel.invokeMethod('triggerPlacesNotification',
        _generateArgumentMap([campaign.campaignId], ['PlacesCampaign']));
  }

  static void triggerPlacesNotificationWithRegion(
      String campaignId, String regionId) {
    _channel.invokeMethod('triggerPlacesNotification',
        _generateArgumentMap([campaignId, regionId], ['long', 'String']));
  }

  static void tagScreen(String screenName) {
    _channel.invokeMethod(
        'tagScreen', _generateArgumentMap([screenName], ['String']));
  }

  // Custom dimensions

  static void setCustomDimension(int dimension, String value) {
    _channel.invokeMethod('setCustomDimension',
        _generateArgumentMap([dimension, value], ['int', 'String']));
  }

  static Future<String> getCustomDimension(int dimension) async {
    return await _channel.invokeMethod(
        'getCustomDimension', _generateArgumentMap([dimension], ['int']));
  }

  static void setAnalyticsListener(AnalyticsListener listener) {
    configure();
    _channel.invokeMethod('setAnalyticsListener',
        _generateArgumentMap([listener != null], ['boolean']));
    _analyticsListener = listener;
  }

  // Profiles

  static void setNumericProfileAttribute(
      String attributeName, int attributeValue,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    _channel.invokeMethod(
        'setProfileAttribute',
        _generateArgumentMap([attributeName, attributeValue, scopeString],
            ['String', 'long', 'ProfileScope']));
  }

  static void setNumericArrayProfileAttribute(
      String attributeName, List<int> attributeValues,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    _channel.invokeMethod(
        'setProfileAttribute',
        _generateArgumentMap([attributeName, attributeValues, scopeString],
            ['String', 'long[]', 'ProfileScope']));
  }

  static void setProfileAttribute(String attributeName, String attributeValue,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    _channel.invokeMethod(
        'setProfileAttribute',
        _generateArgumentMap([attributeName, attributeValue, scopeString],
            ['String', 'String', 'ProfileScope']));
  }

  static void setArrayProfileAttribute(
      String attributeName, List<String> attributeValue,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    _channel.invokeMethod(
        'setProfileAttribute',
        _generateArgumentMap([attributeName, attributeValue, scopeString],
            ['String', 'String[]', 'ProfileScope']));
  }

  static void setDateProfileAttribute(
      String attributeName, DateTime attributeValue,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    _channel.invokeMethod(
        'setProfileAttribute',
        _generateArgumentMap(
            [attributeName, attributeValue.toIso8601String(), scopeString],
            ['String', 'Date', 'ProfileScope']));
  }

  static void setDateArrayProfileAttribute(
      String attributeName, List<DateTime> attributeValue,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    List<String> dates =
        attributeValue.map((e) => e.toIso8601String()).toList();

    _channel.invokeMethod(
        'setProfileAttribute',
        _generateArgumentMap([attributeName, dates, scopeString],
            ['String', 'Date[]', 'ProfileScope']));
  }

  static void addNumericArrayProfileAttributeToSet(
      String attributeName, List<int> attributeValues,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    _channel.invokeMethod(
        'addProfileAttributesToSet',
        _generateArgumentMap([attributeName, attributeValues, scopeString],
            ['String', 'long[]', 'ProfileScope']));
  }

  static void addArrayProfileAttributeToSet(
      String attributeName, List<String> attributeValues,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    _channel.invokeMethod(
        'addProfileAttributesToSet',
        _generateArgumentMap([attributeName, attributeValues, scopeString],
            ['String', 'String[]', 'ProfileScope']));
  }

  static void addDateArrayProfileAttributeToSet(
      String attributeName, List<DateTime> attributeValues,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    List<String> dates =
        attributeValues.map((e) => e.toIso8601String()).toList();
    _channel.invokeMethod(
        'addProfileAttributesToSet',
        _generateArgumentMap([attributeName, dates, scopeString],
            ['String', 'Date[]', 'ProfileScope']));
  }

  static void removeNumericArrayProfileAttributeFromSet(
      String attributeName, List<int> attributeValues,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    _channel.invokeMethod(
        'removeProfileAttributesFromSet',
        _generateArgumentMap([attributeName, attributeValues, scopeString],
            ['String', 'long[]', 'ProfileScope']));
  }

  static void removeArrayProfileAttributeFromSet(
      String attributeName, List<String> attributeValues,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    _channel.invokeMethod(
        'removeProfileAttributesFromSet',
        _generateArgumentMap([attributeName, attributeValues, scopeString],
            ['String', 'String[]', 'ProfileScope']));
  }

  static void removeDateArrayProfileAttributeFromSet(
      String attributeName, List<DateTime> attributeValues,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    List<String> dates =
        attributeValues.map((e) => e.toIso8601String()).toList();
    _channel.invokeMethod(
        'removeProfileAttributesFromSet',
        _generateArgumentMap([attributeName, dates, scopeString],
            ['String', 'Date[]', 'ProfileScope']));
  }

  static void incrementNumericProfileAttribute(
      String attributeName, int incrementValue,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    _channel.invokeMethod(
        'incrementProfileAttribute',
        _generateArgumentMap([attributeName, incrementValue, scopeString],
            ['String', 'long', 'ProfileScope']));
  }

  static void decrementNumericProfileAttribute(
      String attributeName, int decrementValue,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    _channel.invokeMethod(
        'decrementProfileAttribute',
        _generateArgumentMap([attributeName, decrementValue, scopeString],
            ['String', 'long', 'ProfileScope']));
  }

  static void deleteProfileAttribute(String attributeName,
      {ProfileScope scope = ProfileScope.application}) {
    String scopeString = scope.rawValue();
    _channel.invokeMethod(
        'deleteProfileAttribute',
        _generateArgumentMap(
            [attributeName, scopeString], ['String', 'ProfileScope']));
  }

  static void setCustomerEmail(String email) {
    _channel.invokeMethod(
        'setCustomerEmail', _generateArgumentMap([email], ['String']));
  }

  static void setCustomerFirstName(String firstName) {
    _channel.invokeMethod(
        'setCustomerFirstName', _generateArgumentMap([firstName], ['String']));
  }

  static void setCustomerLastName(String lastName) {
    _channel.invokeMethod(
        'setCustomerLastName', _generateArgumentMap([lastName], ['String']));
  }

  static void setCustomerFullName(String fullName) {
    _channel.invokeMethod(
        'setCustomerFullName', _generateArgumentMap([fullName], ['String']));
  }

  // In app messaging

  static void triggerInAppMessage(String message) {
    _channel.invokeMethod(
        'triggerInAppMessage', _generateArgumentMap([message], ['String']));
  }

  static void triggerInAppMessagesForSessionStart() {
    _channel.invokeMethod('triggerInAppMessagesForSessionStart');
  }

  static void triggerInAppMessageWithAttributes(
      String message, Map<String, String> attributes) {
    _channel.invokeMethod('triggerInAppMessage',
        _generateArgumentMap([message, attributes], ['String', 'Map']));
  }

  static void dismissCurrentInAppMessage() {
    _channel.invokeMethod('dismissCurrentInAppMessage');
  }

  // Push notifications

  static void registerPush() {
    _channel.invokeMethod('registerPush');
  }

  static Future<String> getPushRegistrationId() async {
    return await _channel.invokeMethod('getPushRegistrationId');
  }

  static void setPushRegistrationId(String id) {
    _channel.invokeMethod(
        'setPushRegistrationId', _generateArgumentMap([id], ['String']));
  }

  // Test mode

  static void setTestModeEnabled(bool enabled) {
    _channel.invokeMethod(
        'setTestModeEnabled', _generateArgumentMap([enabled], ['boolean']));
  }

  static Future<bool> isTestModeEnabled() async {
    return await _channel.invokeMethod('isTestModeEnabled');
  }

  static void enableLiveDeviceLogging() {
    _channel.invokeMethod('enableLiveDeviceLogging');
  }

  static void setMessagingListener(
      MessagingListener listener,
      InAppConfiguration inAppConfig,
      PushConfiguration pushConfig,
      PushConfiguration placesConfig) {
    configure();
    List<dynamic> args = [
      inAppConfig == null ? null : inAppConfig.toMap(),
      pushConfig == null ? null : pushConfig.toMap(),
      placesConfig == null ? null : placesConfig.toMap()
    ];
    _channel.invokeMethod(
        'setMessagingListener',
        _generateArgumentMap([listener != null, args[0], args[1], args[2]],
            ['boolean', 'InAppConfig', 'PushConfig', 'PushConfig']));
    _messagingListener = listener;
  }

  static void setCallToActionListener(CallToActionListener listener) {
    _channel.invokeMethod('setCallToActionListener',
        _generateArgumentMap([listener != null], ['boolean']));
    _callToActionListener = listener;
  }

  static void setLocationMonitoringEnabled(bool enabled) {
    _channel.invokeMethod('setLocationMonitoringEnabled',
        _generateArgumentMap([enabled], ['boolean']));
  }

  static void setLocationMonitoringEnabledWithPersistance(
      bool enabled, bool persisted) {
    _channel.invokeMethod('setLocationMonitoringEnabled',
        _generateArgumentMap([enabled, persisted], ['boolean', 'boolean']));
  }

  static Future<List<InboxCampaign>> getDisplayableInboxCampaigns() async {
    List<dynamic> rawCampaigns = await _channel.invokeMethod(
        'getDisplayableInboxCampaigns',
        _generateArgumentMap([], [], returnType: 'List<InboxCampaign>'));
    List<InboxCampaign> campaigns = rawCampaigns.map((e) {
      Map<dynamic, dynamic> rawCampaign = e;
      return InboxCampaign.fromMap(rawCampaign);
    }).toList();
    return campaigns;
  }

  static Future<List<InboxCampaign>> getAllInboxCampaigns() async {
    List<dynamic> rawCampaigns = await _channel.invokeMethod(
        'getAllInboxCampaigns',
        _generateArgumentMap([], [], returnType: 'List<InboxCampaign>'));
    List<InboxCampaign> campaigns = rawCampaigns.map((e) {
      Map<dynamic, dynamic> rawCampaign = e;
      return InboxCampaign.fromMap(rawCampaign);
    }).toList();
    return campaigns;
  }

  static void setInboxCampaignRead(InboxCampaign campaign, bool read) {
    _channel.invokeMethod(
        'setInboxCampaignRead',
        _generateArgumentMap(
            [campaign.campaignId, read], ['InboxCampaign', 'boolean']));
  }

  static void deleteInboxCampaing(InboxCampaign campaign) {
    _channel.invokeMethod('deleteInboxCampaign',
        _generateArgumentMap([campaign.campaignId], ['InboxCampaign']));
  }

  static Future<int> getInboxCampaignUnreadCount() async {
    return await _channel.invokeMethod('getInboxCampaignsUnreadCount');
  }

  static void inboxListItemTapped(InboxCampaign campaign) {
    _channel.invokeMethod('inboxListItemTapped',
        _generateArgumentMap([campaign.campaignId], ['InboxCampaign']));
  }

  // Location

  static Future<List<CircularRegion>> getGeofencesToMonitor(
      double longitude, double latitude) async {
    List<dynamic> rawRegions = await _channel.invokeMethod(
        'getGeofencesToMonitor',
        _generateArgumentMap([longitude, latitude], ['double', 'double'],
            returnType: "List<CircularRegion>"));

    return CircularRegion.listFromListOfMaps(rawRegions);
  }

  static void triggerRegion(
      Region region, RegionEvent event, Location location) {
    _channel.invokeMethod(
        'triggerRegion',
        _generateArgumentMap(
            [region.toMap(), event.rawValue(), location.toMap()],
            ['Region', 'Region.Event', 'Location']));
  }

  static void triggerRegions(
      List<Region> regions, RegionEvent event, Location location) {
    _channel.invokeMethod(
        'triggerRegions',
        _generateArgumentMap([
          regions.map((e) => e.toMap()).toList(),
          event.rawValue(),
          location.toMap()
        ], [
          'List<Region>',
          'Region.Event',
          'Location'
        ]));
  }

  static void setLocationListener(LocationListener listener) {
    configure();
    _channel.invokeMethod('setLocationListener',
        _generateArgumentMap([listener != null], ['boolean']));
    _locationListener = listener;
  }

  static void setIdentifier(String key, String value) {
    _channel.invokeMethod('setIdentifier',
        _generateArgumentMap([key, value], ['String', 'String']));
  }

  static void setCustomerId(String id) {
    _channel.invokeMethod(
        'setCustomerId', _generateArgumentMap([id], ['String']));
  }

  static void setCustomerIdWithPrivacyOptedOut(
      String customerId, bool optedOut) {
    _channel.invokeMethod('setCustomerIdWithPrivacyOptedOut',
        _generateArgumentMap([customerId, optedOut], ['String', 'boolean']));
  }

  static Future<String> getCustomerId() async {
    return await _channel.invokeMethod('getCustomerId');
  }

  static Future<String> getIdentifier(String key) async {
    return await _channel.invokeMethod(
        'getIdentifier', _generateArgumentMap([key], ['String']));
  }

  static void setLocation(Location location) {
    _channel.invokeMethod(
        'setLocation', _generateArgumentMap([location.toMap()], ['Location']));
  }

  static void setLoggingEnabled(bool enabled) {
    _channel.invokeMethod(
        'setLoggingEnabled', _generateArgumentMap([enabled], ['boolean']));
  }

  // TODO: add iOS counterpart
  static void redirectLogsToDisk(bool writeExternally) {
    _channel.invokeListMethod('redirectLogsToDisk',
        _generateArgumentMap([writeExternally], ['boolean', 'Context']));
  }

  static Future<bool> isLoggingEnabled() async {
    return await _channel.invokeMethod('isLoggingEnabled');
  }

  static Future<String> getInstallId() async {
    return await _channel.invokeMethod('getInstallId');
  }

  static Future<String> getLocalAuthenticationToken() async {
    return await _channel.invokeMethod('getLocalAuthenticationToken');
  }

  static Future<String> getAppKey() async {
    return await _channel.invokeMethod('getAppKey');
  }

  static Map<String, dynamic> _generateArgumentMap(
      List<Object> values, List<String> types,
      {String returnType = "automatic", List<String> platform}) {
    return LocalyticsBase._generateArgumentMap(values, types,
        returnType: returnType, platform: platform);
  }
}

class LocalyticsAndroid {
  static const MethodChannel _channel = LocalyticsBase._channel;

  static void appendAdidToInAppUrls(bool enabled) {
    _channel.invokeMethod('appendAdidToInAppUrls',
        _generateArgumentMap([enabled], ['boolean'], platform: ['android']));
  }

  static Future<bool> isAdidAppendedToInAppUrls() async {
    return await _channel.invokeMethod('isAdidAppendedToInAppUrls',
        _generateArgumentMap([], [], platform: ['android']));
  }

  static void appendAdidToInboxUrls(bool enabled) {
    _channel.invokeMethod('appendAdidToInboxUrls',
        _generateArgumentMap([enabled], ['boolean'], platform: ['android']));
  }

  static Future<bool> isAdidAppendedToInboxUrls() async {
    return await _channel.invokeMethod('isAdidAppendedToInboxUrls',
        _generateArgumentMap([], [], platform: ['android']));
  }

  static void setNotificationsDisabled(bool disabled) {
    _channel.invokeMethod('setNotificationsDisabled',
        _generateArgumentMap([disabled], ['boolean'], platform: ['android']));
  }

  static Future<bool> areNotificationsDisabled() async {
    return await _channel.invokeMethod('areNotificationsDisabled',
        _generateArgumentMap([], [], platform: ['android']));
  }

  static Future<bool> handleFirebaseMessage(
      Map<String, String> messageData) async {
    return await _channel.invokeMethod('handleFirebaseMessage',
        _generateArgumentMap([messageData], ['Map'], platform: ['android']));
  }

  static void tagPushReceivedEvent(Map<String, String> data) {
    _channel.invokeMethod('tagPushReceivedEvent',
        _generateArgumentMap([data], ['Map'], platform: ['android']));
  }

  static Map<String, dynamic> _generateArgumentMap(
      List<Object> values, List<String> types,
      {String returnType = "automatic", List<String> platform}) {
    return LocalyticsBase._generateArgumentMap(values, types,
        returnType: returnType, platform: platform);
  }
}

class LocalyticsIOS {
  static const MethodChannel _channel = LocalyticsBase._channel;

  static Future<bool> requestAdvertisingIdentifierPrompt() async {
    return await _channel.invokeMethod('requestAdvertisingIdentifierPrompt',
        _generateArgumentMap([], [], platform: ['iOS']));
  }

  static Future<AdIdStatus> advertisingIdentifierStatus() async {
    int raw = await _channel.invokeMethod('advertisingIdentifierStatus',
        _generateArgumentMap([], [], platform: ['iOS']));
    return AdIdStatusExtension.convert(raw);
  }

  static Map<String, dynamic> _generateArgumentMap(
      List<Object> values, List<String> types,
      {String returnType = "automatic", List<String> platform}) {
    return LocalyticsBase._generateArgumentMap(values, types,
        returnType: returnType, platform: platform);
  }
}
