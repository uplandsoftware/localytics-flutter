import 'Region.dart';

class CircularRegion extends Region {
  int radius;

  Map<String, dynamic> toMap() {
    Map<String, dynamic> parentMap = super.toMap();
    parentMap['radius'] = radius;
    return parentMap;
  }

  static CircularRegion fromMap(Map<dynamic, dynamic> raw) {
    CircularRegion r = new CircularRegion();
    r.placeId = raw['placeId'];
    r.schemaVersion = raw['schemaVersion'];
    r.uniqueId = raw['uniqueId'];
    r.name = raw['name'];
    r.type = raw['type'];
    r.radius = raw['radius'];
    r.latitude = raw['latitude'];
    r.longitude = raw['longitude'];
    r.enterAnalyticsEnabled = raw['enterAnalyticsEnabled'];
    r.exitAnalyticsEnabled = raw['exitAnalyticsEnabled'];
    Map<dynamic, dynamic> rawAttribtues = raw['attributes'];
    if (rawAttribtues != null) {
      r.attributes = rawAttribtues.map((rkey, rvalue) {
        String key = rkey;
        String value = rvalue;
        return MapEntry<String, String>(key, value);
      });
    }
    r.radius = raw['radius'];
    return r;
  }

  static List<CircularRegion> listFromListOfMaps(List<dynamic> raw) {
    return raw.map((e) {
      Map<dynamic, dynamic> rawRegion = e;
      return CircularRegion.fromMap(rawRegion);
    }).toList();
  }
}
