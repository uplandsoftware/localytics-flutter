class Customer {
  String customerId;
  String firstName;
  String lastName;
  String fullName;
  String emailAddress;

  Map<String, String> toMap() {
    return {
      'customerId': customerId,
      'firstName': firstName,
      'lastName': lastName,
      'fullName': fullName,
      'emailAddress': emailAddress
    };
  }
}
