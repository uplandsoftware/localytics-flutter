enum RegionEvent { enter, exit }

extension RegionEventExtension on RegionEvent {
  String rawValue() {
    switch (this) {
      case RegionEvent.enter:
        {
          return "enter";
        }
      case RegionEvent.exit:
        {
          return "exit";
        }
    }
  }

  static RegionEvent fromString(String str) {
    if (str == "enter") {
      return RegionEvent.enter;
    } else {
      return RegionEvent.exit;
    }
  }
}
