class PushConfiguration {
  bool shouldShow;
  bool diy;
  String category;
  int color;
  String contentInfo;
  String contentTitle;
  List<String> defaults;
  int priority;
  String sound;
  List<int> vibrate;

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      "shouldShow": shouldShow,
      "category": category,
      "color": color,
      "contentInfo": contentInfo,
      "contentTitle": contentTitle,
      "defaults": defaults,
      "priority": priority,
      "sound": sound,
      "vibrate": vibrate
    };
  }
}
