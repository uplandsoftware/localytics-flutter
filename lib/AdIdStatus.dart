enum AdIdStatus { notDetermined, restricted, denied, authorized, notRequired }

extension AdIdStatusExtension on AdIdStatus {
  static AdIdStatus convert(int raw) {
    switch (raw) {
      case 0:
        {
          return AdIdStatus.notDetermined;
        }
      case 1:
        {
          return AdIdStatus.restricted;
        }
      case 2:
        {
          return AdIdStatus.denied;
        }
      case 3:
        {
          return AdIdStatus.authorized;
        }
      case 4:
        {
          return AdIdStatus.notRequired;
        }
      default:
        {
          return AdIdStatus.notDetermined;
        }
    }
  }
}
