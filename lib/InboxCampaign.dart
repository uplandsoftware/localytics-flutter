import 'package:flutter/foundation.dart';

import 'Campaign.dart';

class InboxCampaign extends Campaign {
  bool isRead;
  bool isVisible;
  bool isDeleted;
  bool isPushToInboxCampaign;
  String title;
  int sortOrder;

  /// ISO date on android, unix timestamp on iOS
  String receivedDate;
  String summary;
  bool hasThumbnail;
  String thumbnailUri;
  bool hasCreative;
  String deepLinkUrl;

  static InboxCampaign fromMap(Map<dynamic, dynamic> raw) {
    InboxCampaign c = new InboxCampaign();
    c.campaignId = raw['campaignId'];
    c.name = raw['ruleName'];
    Map<dynamic, dynamic> rawAttribtues = raw['attributes'];
    if (rawAttribtues != null) {
      c.attributes = rawAttribtues.map((rkey, rvalue) {
        String key = rkey;
        String value = rvalue;
        return MapEntry<String, String>(key, value);
      });
    }
    c.isRead = raw['read'];
    c.isVisible = raw['visible'];
    c.isDeleted = raw['deleted'];
    c.isPushToInboxCampaign = raw['isPushToInboxCampaign'];
    c.title = raw['title'];
    c.sortOrder = raw['sortOrder'];
    if (defaultTargetPlatform == TargetPlatform.android) {
      c.receivedDate = raw['receivedDate'];
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      c.receivedDate = raw['receivedDate'].toString();
    }
    c.summary = raw['summary'];
    c.hasThumbnail = raw['hasThumbnail'];
    c.thumbnailUri = raw['thumbnailUri'];
    c.hasCreative = raw['hasCreative'];
    c.deepLinkUrl = raw['deepLinkUrl'];
    return c;
  }
}
