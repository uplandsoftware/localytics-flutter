class Region {
  int placeId, schemaVersion;
  String uniqueId, name, type;
  double latitude, longitude;
  bool enterAnalyticsEnabled, exitAnalyticsEnabled;
  Map<String, String> attributes;

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'placeId': placeId,
      'schemaVersion': schemaVersion,
      'uniqueId': uniqueId,
      'name': name,
      'type': type,
      'latitude': latitude,
      'longitude': longitude,
      'enterAnalyticsEnabled': enterAnalyticsEnabled,
      'exitAnalyticsEnabled': exitAnalyticsEnabled,
      'attributes': attributes
    };
  }
}
