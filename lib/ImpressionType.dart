enum ImpressionType { click, dismiss }

extension ImpressionTypeExtention on ImpressionType {
  String rawValue() {
    switch (this) {
      case ImpressionType.click:
        {
          return "click";
        }
      case ImpressionType.dismiss:
        {
          return "dismiss";
        }
    }
  }
}
