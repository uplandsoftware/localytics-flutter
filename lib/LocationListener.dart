import 'CircularRegion.dart';
import 'Location.dart';
import 'RegionEvent.dart';

class LocationListener {
  void localyticsDidUpdateLocation(Location location) {}
  void localyticsDidTriggerRegions(
      List<CircularRegion> regions, RegionEvent event) {}
  void localyticsDidUpdateMonitoredGeofences(
      List<CircularRegion> added, List<CircularRegion> removed) {}
}
