import 'InAppCampaign.dart';
import 'InboxCampaign.dart';
import 'PlacesCampaign.dart';
import 'PushCampaign.dart';

class Campaign {
  int campaignId;
  String name;
  Map<String, String> attributes;

  static Campaign fromMap(Map<dynamic, dynamic> raw) {
    String classType = raw['classType'];
    if (classType == 'InAppCampaign') {
      return InAppCampaign.fromMap(raw);
    } else if (classType == 'InboxCampaign') {
      return InboxCampaign.fromMap(raw);
    } else if (classType == 'PushCampaign') {
      return PushCampaign.fromMap(raw);
    } else if (classType == 'PlacesCampaign') {
      return PlacesCampaign.fromMap(raw);
    }
    return null; // should never happen
  }
}
