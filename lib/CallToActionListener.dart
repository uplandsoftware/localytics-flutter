import 'Campaign.dart';

class CallToActionListener {
  void localyticsShouldDeeplinkToSettings(Campaign campaign) {}
  void localyticsShouldDeeplink(String url, Campaign campaign) {}
  void localyticsDidOptOut(bool optOut, Campaign campaign) {}
  void localyticsDidPrivacyOptOut(bool optOut, Campaign campaign) {}

  // android specific
  void localyticsShouldPromptForLocationPermissions(Campaign campaign) {}

  // iOS specific
  void localyticsShouldPromptForLocationWhenInUsePermissions(
      Campaign campaign) {}
  void localyticsShouldPromptForLocationAlwaysPermissions(Campaign campaign) {}
  void localyticsShouldPromptForNotificationPermissions(Campaign campaign) {}
}
