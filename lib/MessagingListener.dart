import 'localytics.dart';
import 'InAppCampaign.dart';
import 'PlacesCampaign.dart';
import 'PushCampaign.dart';

class MessagingListener {
  void localyticsShouldShowInAppMessage(InAppCampaign campaign) {}
  void localyticsWillDisplayInAppMessage(InAppCampaign campaign) {}
  void localyticsDidDisplayInAppMessage() {}
  void localyticsWillDismissInAppMessage() {}
  void localyticsDidDismissInAppMessage() {}
  void localyticsShouldDelaySessionStartInAppMessages(bool shouldDelay) {}
  void localyticsShouldShowPushNotification(PushCampaign campaign) {}
  void localyticsWillShowPushNotification(PushCampaign campaign) {}
  void localyticsShouldShowPlacesPushNotification(PlacesCampaign campaign) {}
  void localyticsWillShowPlacesPushNotification(PlacesCampaign campaign) {}
}
