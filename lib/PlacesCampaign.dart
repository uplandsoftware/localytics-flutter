import 'Campaign.dart';
import 'CircularRegion.dart';
import 'RegionEvent.dart';

class PlacesCampaign extends Campaign {
  // Places campaign
  int creativeId;
  String creativeType;
  String message;
  String soundFilename;
  String attachmentUrl;
  String attachmentType;
  RegionEvent triggerEvent;
  CircularRegion region;

  static PlacesCampaign fromMap(Map<dynamic, dynamic> m) {
    PlacesCampaign c = new PlacesCampaign();
    c.campaignId = m['campaignId'];
    c.name = m['name'];
    Map<dynamic, dynamic> rawAttribtues = m['attributes'];
    if (rawAttribtues != null) {
      c.attributes = rawAttribtues.map((rkey, rvalue) {
        String key = rkey;
        String value = rvalue;
        return MapEntry<String, String>(key, value);
      });
    }
    c.creativeId = m['creativeId'];
    c.creativeType = m['creativeType'];
    c.message = m['message'];
    c.soundFilename = m['soundFilename'];
    c.attachmentUrl = m['attachmentUrl'];
    c.triggerEvent = RegionEventExtension.fromString(m['triggerEvent']);
    c.region = CircularRegion.fromMap(m['region']);
    return c;
  }
}
