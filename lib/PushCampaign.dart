import 'Campaign.dart';

class PushCampaign extends Campaign {
  // Push campaign
  String title;
  int creativeId;
  String creativeType;
  String message;
  String soundFilename;
  String attachmentUrl;

  static PushCampaign fromMap(Map<dynamic, dynamic> m) {
    PushCampaign c = new PushCampaign();
    c.campaignId = m['campaignId'];
    c.name = m['name'];
    Map<dynamic, dynamic> rawAttribtues = m['attributes'];
    if (rawAttribtues != null) {
      c.attributes = rawAttribtues.map((rkey, rvalue) {
        String key = rkey;
        String value = rvalue;
        return MapEntry<String, String>(key, value);
      });
    }
    c.title = m['title'];
    c.creativeId = m['creativeId'];
    c.creativeType = m['creativeType'];
    c.message = m['message'];
    c.soundFilename = m['soundFilename'];
    c.attachmentUrl = m['attachmentUrl'];
    return c;
  }
}
