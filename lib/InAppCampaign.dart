import 'Campaign.dart';

class InAppCampaign extends Campaign {
  String creativeFilePath;
  double aspectRatio;
  int bannerOffsetDps;
  double backgroundAlpha;
  String displayLocation;
  bool isDismissButtonHidden;
  String dismissButtonLocation;
  String eventName;
  Map<String, String> eventAttributes;

  static InAppCampaign fromMap(Map<dynamic, dynamic> m) {
    InAppCampaign c = new InAppCampaign();
    c.campaignId = m['campaignId'];
    c.name = m['name'];
    Map<dynamic, dynamic> rawAttribtues = m['attributes'];
    if (rawAttribtues != null) {
      c.attributes = rawAttribtues.map((rkey, rvalue) {
        String key = rkey;
        String value = rvalue;
        return MapEntry<String, String>(key, value);
      });
    }
    c.creativeFilePath = m['creativeFilePath'];
    c.aspectRatio = m['aspectRatio'];
    c.bannerOffsetDps = m['bannerOffsetDps'];
    c.backgroundAlpha = m['backgroundAlpha'];
    c.displayLocation = m['displayLocation'];
    c.isDismissButtonHidden = m['dismissButtonHidden'];
    c.dismissButtonLocation = m['dismissButtonLocation'];
    c.eventName = m['eventName'];
    Map<dynamic, dynamic> rawEventAttribtues = m['eventAttributes'];
    if (rawEventAttribtues != null) {
      c.eventAttributes = rawEventAttribtues.map((rkey, rvalue) {
        String key = rkey;
        String value = rvalue;
        return MapEntry<String, String>(key, value);
      });
    }
    return c;
  }
}
