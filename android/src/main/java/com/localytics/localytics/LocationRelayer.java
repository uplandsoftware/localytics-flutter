package com.localytics.localytics;

import android.location.Location;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.localytics.android.CircularRegion;
import com.localytics.android.LocationListener;
import com.localytics.android.Region;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import io.flutter.plugin.common.MethodChannel;

public class LocationRelayer implements LocationListener {

    MethodChannel channel;
    Handler handler;

    LocationRelayer(MethodChannel channel, Handler handler) {
        this.channel = channel;
        this.handler = handler;
    }

    @Override
    public void localyticsDidUpdateLocation(@Nullable final Location location) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsDidUpdateLocation", Translator.translate(location));
            }
        });
    }

    @Override
    public void localyticsDidTriggerRegions(@NonNull List<Region> regions, @NonNull Region.Event event) {
        final List<Object> args = Arrays.asList(Translator.translate(regions), Translator.translate(event));
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsDidTriggerRegions", args);
            }
        });
    }

    @Override
    public void localyticsDidUpdateMonitoredGeofences(@NonNull List<CircularRegion> added, @NonNull List<CircularRegion> removed) {
        final List<List<Map<String, Object>>> args = Arrays.asList(Translator.translate(added), Translator.translate(removed));
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsDidUpdateMonitoredGeofences", args);
            }
        });
    }
}
