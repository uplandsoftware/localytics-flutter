package com.localytics.localytics;

import android.app.Notification;
import android.net.Uri;
import android.os.Handler;
import android.util.LongSparseArray;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.localytics.android.InAppCampaign;
import com.localytics.android.InAppConfiguration;
import com.localytics.android.MessagingListenerV2;
import com.localytics.android.PlacesCampaign;
import com.localytics.android.PushCampaign;

import java.util.List;
import java.util.Map;

import io.flutter.plugin.common.MethodChannel;

public class MessagingRelayer implements MessagingListenerV2 {

    static class InAppConfig {
        Boolean shouldShow;
        Boolean diy;
        Double aspectRatio;
        Double backgroundAlpha;
        Integer bannerOffsetDps;
        String dismissButtonLocation;
        Boolean dismissButtonHidden;
        Double videoConversionPercentage;
        Boolean delaySessionStart;
    }

    static class PushConfig {
        Boolean shouldShow;
        Boolean diy;
        String category;
        Integer color;
        String contentInfo;
        String contentTitle;
        List<String> defaults;
        Integer priority;
        String sound;
        List<Integer> vibrate;
    }

    private LongSparseArray<InAppCampaign> inAppCampaignCache;
    private LongSparseArray<PushCampaign> pushCampaignCache;
    private LongSparseArray<PlacesCampaign> placesCampaignCache;
    private MethodChannel channel;
    private Handler handler;
    private InAppConfig inAppConfig;
    private PushConfig pushConfig;
    private PushConfig placesConfig;

    public MessagingRelayer(MethodChannel channel, Handler handler, LongSparseArray<InAppCampaign> inAppCampaignCache, LongSparseArray<PushCampaign> pushCampaignCache, LongSparseArray<PlacesCampaign> placesCampaignCache) {
        this.channel = channel;
        this.handler = handler;
        this.inAppCampaignCache = inAppCampaignCache;
        this.pushCampaignCache = pushCampaignCache;
        this.placesCampaignCache = placesCampaignCache;
    }

    public void setInAppConfig(InAppConfig inAppConfig) {
        this.inAppConfig = inAppConfig;
    }

    public void setPushConfig(PushConfig pushConfig) {
        this.pushConfig = pushConfig;
    }

    public void setPlacesConfig(PushConfig placesConfig) {
        this.placesConfig = placesConfig;
    }

    @Override
    public boolean localyticsShouldShowInAppMessage(@NonNull InAppCampaign campaign) {
        // Cache campaign
        inAppCampaignCache.put(campaign.getCampaignId(), campaign);

        boolean shouldShow = inAppConfig == null || (inAppConfig.shouldShow == null ? true : inAppConfig.shouldShow);
        final Map<String, Object> rawCampaign = Translator.translate(campaign);
        if (inAppConfig != null) {
            if (inAppConfig.diy != null && inAppConfig.diy) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        channel.invokeMethod("localyticsDiyInAppMessage", rawCampaign);
                    }
                });

                return false;
            }
        }

        rawCampaign.put("shouldShow", shouldShow);
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsShouldShowInAppMessage", rawCampaign);
            }
        });

        return shouldShow;
    }

    @NonNull
    @Override
    public InAppConfiguration localyticsWillDisplayInAppMessage(@NonNull InAppCampaign inAppCampaign, @NonNull InAppConfiguration configuration) {

        if (inAppConfig != null) {
            if (inAppConfig.aspectRatio != null) {
                configuration.setAspectRatio(inAppConfig.aspectRatio.floatValue());
            }
            if (inAppConfig.backgroundAlpha != null) {
                configuration.setBackgroundAlpha(inAppConfig.backgroundAlpha.floatValue());
            }
            if (inAppConfig.bannerOffsetDps != null) {
                configuration.setBannerOffsetDps(inAppConfig.bannerOffsetDps);
            }
            if (inAppConfig.dismissButtonLocation != null) {
                String location = inAppConfig.dismissButtonLocation;
                configuration.setDismissButtonLocation(Translator.translateDismissButtonLocation(location));
            }
            if (inAppConfig.dismissButtonHidden != null) {
                boolean hidden = inAppConfig.dismissButtonHidden;
                configuration.setDismissButtonVisibility(hidden ? View.GONE : View.VISIBLE);
            }
            if (inAppConfig.videoConversionPercentage != null) {
                configuration.setVideoConversionPercentage(inAppConfig.videoConversionPercentage.floatValue());
            }
        }
        final Map<String, Object> rawCampaign = Translator.translate(inAppCampaign);
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsWillDisplayInAppMessage", rawCampaign);
            }
        });

        return configuration;
    }

    @Override
    public void localyticsDidDisplayInAppMessage() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsDidDisplayInAppMessage", null);
            }
        });
    }

    @Override
    public void localyticsWillDismissInAppMessage() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsWillDismissInAppMessage", null);
            }
        });
    }

    @Override
    public void localyticsDidDismissInAppMessage() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsDidDismissInAppMessage", null);
            }
        });
    }

    @Override
    public boolean localyticsShouldDelaySessionStartInAppMessages() {
        boolean shouldDelay = false;
        if (inAppConfig != null && inAppConfig.delaySessionStart != null) {
            shouldDelay = inAppConfig.delaySessionStart;
        }

        final boolean finalShouldDelay = shouldDelay;
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsShouldDelaySessionStartInAppMessages", finalShouldDelay);
            }
        });


        return shouldDelay;
    }

    @Override
    public boolean localyticsShouldShowPushNotification(@NonNull PushCampaign campaign) {
        // Cache campaign
        pushCampaignCache.put(campaign.getCampaignId(), campaign);

        boolean shouldShow = true;
        Map<String, Object> params = Translator.translate(campaign);

        if (pushConfig != null) {

            // Global Suppression
            if (pushConfig.shouldShow != null) {
                shouldShow = pushConfig.shouldShow;
            }

            // DIY Push. This callback will suppress the push and emit an event
            // for manually handling
            if (pushConfig.diy != null && pushConfig.diy) {
                channel.invokeMethod("localyticsDiyPushNotification", params);

                return false;
            }
        }

        params.put("shouldShow", shouldShow);
        channel.invokeMethod("localyticsShouldShowPushNotification", params);

        return shouldShow;
    }

    @NonNull
    @Override
    public NotificationCompat.Builder localyticsWillShowPushNotification(@NonNull NotificationCompat.Builder builder, @NonNull PushCampaign campaign) {
        if (pushConfig != null) {
            updateNotification(builder, pushConfig);
        }

        Map<String, Object> params = Translator.translate(campaign);
        channel.invokeMethod("localyticsWillShowPushNotification", params);

        return builder;
    }

    @Override
    public boolean localyticsShouldShowPlacesPushNotification(@NonNull PlacesCampaign campaign) {
        // Cache campaign
        placesCampaignCache.put(campaign.getCampaignId(), campaign);

        boolean shouldShow = true;
        Map<String, Object> params = Translator.translate(campaign);

        if (placesConfig != null) {

            // Global Suppression
            if (placesConfig.shouldShow != null) {
                shouldShow = placesConfig.shouldShow;
            }

            // DIY Places. This callback will suppress the places push and emit an event
            // for manually handling
            if (placesConfig.diy != null && placesConfig.diy) {
                channel.invokeMethod("localyticsDiyPlacesPushNotification", params);

                return false;
            }
        }

        params.put("shouldShow", shouldShow);
        channel.invokeMethod("localyticsShouldShowPlacesPushNotification", params);

        return shouldShow;
    }

    @NonNull
    @Override
    public NotificationCompat.Builder localyticsWillShowPlacesPushNotification(@NonNull NotificationCompat.Builder builder, @NonNull PlacesCampaign campaign) {
        if (placesConfig != null) {
            updateNotification(builder, placesConfig);
        }

        Map<String, Object> params = Translator.translate(campaign);

        channel.invokeMethod("localyticsWillShowPlacesPushNotification", params);

        return builder;
    }

    // Helpers

    private NotificationCompat.Builder updateNotification(NotificationCompat.Builder builder, PushConfig config) {
        if (config.category != null) {
            builder.setCategory(config.category);
        }
        if (config.color != null) {
            builder.setColor(config.color);
        }
        if (config.contentInfo != null) {
            builder.setContentInfo(config.contentInfo);
        }
        if (config.contentTitle != null) {
            builder.setContentTitle(config.contentTitle);
        }
        if (config.defaults != null) {
            List<String> defaultsList = config.defaults;
            if (defaultsList.contains("all")) {
                builder.setDefaults(Notification.DEFAULT_ALL);
            } else {
                int defaults = 0;
                if (defaultsList.contains("lights")) {
                    defaults |= Notification.DEFAULT_LIGHTS;
                }
                if (defaultsList.contains("sound")) {
                    defaults |= Notification.DEFAULT_SOUND;
                }
                if (defaultsList.contains("vibrate")) {
                    defaults |= Notification.DEFAULT_VIBRATE;
                }
                builder.setDefaults(defaults);
            }
        }
        if (config.priority != null) {
            builder.setPriority(config.priority);
        }
        if (config.sound != null) {
            builder.setSound(Uri.parse(config.sound));
        }
        if (config.vibrate != null) {
            List<Integer> vibrateArray = config.vibrate;
            int size = vibrateArray.size();
            long[] vibrate = new long[size];
            for (int i = 0; i < size; i++) {
                vibrate[i] = (long) vibrateArray.get(i);
            }
            builder.setVibrate(vibrate);
        }

        return builder;
    }
}
