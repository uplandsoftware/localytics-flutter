package com.localytics.localytics;

import android.os.Handler;

import com.localytics.android.AnalyticsListener;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import io.flutter.plugin.common.MethodChannel;

public class AnalyticsRelayer implements AnalyticsListener {
    MethodChannel channel;
    Handler handler;

    AnalyticsRelayer(MethodChannel channel, Handler handler) {
        this.channel = channel;
        this.handler = handler;
    }

    @Override
    public void localyticsSessionWillOpen(boolean isFirst, boolean isUpgrade, boolean isResume) {
        final List<Boolean> args = Arrays.asList(isFirst, isUpgrade, isResume);
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsSessionWillOpen", args);
            }
        });
    }

    @Override
    public void localyticsSessionDidOpen(boolean isFirst, boolean isUpgrade, boolean isResume) {
        final List<Boolean> args = Arrays.asList(isFirst, isUpgrade, isResume);
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsSessionDidOpen", args);
            }
        });
    }

    @Override
    public void localyticsSessionWillClose() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsSessionWillClose", null);
            }
        });
    }

    @Override
    public void localyticsDidTagEvent(String event, Map<String, String> attributes, long customerValueIncrease) {
        final List<Object> args = Arrays.asList(event, (Map<String, String>) attributes, customerValueIncrease);
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsDidTagEvent", args);
            }
        });
    }
}
