package com.localytics.localytics;

import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.LongSparseArray;

import androidx.annotation.NonNull;

import com.localytics.android.CircularRegion;
import com.localytics.android.Customer;
import com.localytics.android.InAppCampaign;
import com.localytics.android.InboxCampaign;
import com.localytics.android.Localytics;
import com.localytics.android.PlacesCampaign;
import com.localytics.android.PushCampaign;
import com.localytics.android.Region;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/** LocalyticsPlugin */
public class LocalyticsPlugin implements FlutterPlugin, MethodCallHandler {

  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;
  private Context context;
  private ExecutorService es = Executors.newCachedThreadPool();
  private Handler mainHandler = new Handler(Looper.getMainLooper());
  private LongSparseArray<InboxCampaign> inboxCampaignCache = new LongSparseArray<>();
  private LongSparseArray<InAppCampaign> inAppCampaignCache = new LongSparseArray<>();
  private LongSparseArray<PushCampaign> pushCampaignCache = new LongSparseArray<>();
  private LongSparseArray<PlacesCampaign> placesCampaignCache = new LongSparseArray<>();
  private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "localytics");
    context = flutterPluginBinding.getApplicationContext();
    channel.setMethodCallHandler(this);
  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  public static void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), "localytics");
    channel.setMethodCallHandler(new LocalyticsPlugin());
  }

  @Override
  public void onMethodCall(@NonNull final MethodCall call, @NonNull final Result result) {
    final Map<String, List<Object>> args = (Map<String, List<Object>>) call.arguments;
    if (args != null) {
      List<Object> platforms = args.get("platform");
      if (platforms != null && !platforms.contains("android")) {
        return;
      }
    }
    if (call.method.equals("setAnalyticsListener")) {
      if (!((boolean) args.get("values").get(0))) {
        Localytics.setAnalyticsListener(null);
        return;
      }
      Localytics.setAnalyticsListener(new AnalyticsRelayer(channel, mainHandler));
    } else if (call.method.equals("setLocationListener")) {
      if (!((boolean) args.get("values").get(0))) {
        Localytics.setLocationListener(null);
        return;
      }
      Localytics.setLocationListener(new LocationRelayer(channel, mainHandler));
    } else if (call.method.equals("setMessagingListener")) {
      if (!((boolean) args.get("values").get(0))) {
        Localytics.setMessagingListener(null);
        return;
      }
      MessagingRelayer relayer = new MessagingRelayer(channel, mainHandler, inAppCampaignCache, pushCampaignCache, placesCampaignCache);
      Localytics.setMessagingListener(relayer);
      List<Object> configs = (List<Object>) args.get("values");

      if (configs.get(1) != null) {
        relayer.setInAppConfig(toConfig((Map<String, Object>) configs.get(1)));
      }
      if (configs.get(2) != null) {
        relayer.setPushConfig(toPushConfig((Map<String, Object>) configs.get(2)));
      }
      if (configs.get(3) != null) {
        relayer.setPlacesConfig(toPushConfig((Map<String, Object>) configs.get(3)));
      }
    } else if (call.method.equals("setCallToActionListener")) {
      if (!((boolean) args.get("values").get(0))) {
        Localytics.setCallToActionListener(null);
        return;
      }
      Localytics.setCallToActionListener(new CallToActionRelayer(channel, mainHandler));
    } else if (args != null && args.get("values").size() > 0) {
      Runnable runner = new Runnable() {
        @Override
        public void run() {
          try {
            HarmonizedArgumentsAndTypes ht = generateTypeArray(args);
            List<Class> types = ht.types;
            List<Object> argValues = ht.arguments;

            Class[] rawTypes = new Class[types.size()];
            types.toArray(rawTypes);

            Method m = Localytics.class.getMethod(call.method, rawTypes);
            Object raw = m.invoke(null, argValues.toArray());
            final Object r = convertReturnType((String) call.argument("returnType"), raw);
            mainHandler.post(new Runnable() {
              @Override
              public void run() {
                  result.success(r);
              }
            });
          } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
          }
        }
      };
      es.submit(runner);
    } else {
        if (call.arguments == null || args.get("values").size() == 0) {
          Runnable runner = new Runnable() {
            @Override
            public void run() {
              try {
                Method m = Localytics.class.getMethod(call.method);
                Object raw = m.invoke(this);
                final Object r = convertReturnType((String) call.argument("returnType"), raw);
                mainHandler.post(new Runnable() {
                  @Override
                  public void run() {
                    result.success(r);
                  }
                });
              } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
              }
            }
          };
          es.submit(runner);
        } else {
          System.out.println("uups");
          result.notImplemented();
        }
    }
  }

  Object convertReturnType(String type, Object value) {
    if (type == null || type.equals("automatic") ) {
      return value;
    } else {
      switch (type) {
        case "List<CircularRegion>":
          List<CircularRegion> regions = (List<CircularRegion>) value;
          List<? extends Region> castRegions = regions;
          return Translator.translate(castRegions);
        case "List<InboxCampaign>":
          List<InboxCampaign> campaigns = (List<InboxCampaign>) value;

          // cache the campaigns
          for (InboxCampaign campaign : campaigns) {
            inboxCampaignCache.put(campaign.getCampaignId(), campaign);
          }

          List<Map<String, Object>> rawCampaigns = new ArrayList<>();
          for (InboxCampaign campaign : campaigns) {
            rawCampaigns.add(Translator.translate(campaign));
          }
          return rawCampaigns;
        default:
          //TODO: crash?
          return null;
      }
    }
  }

  private class HarmonizedArgumentsAndTypes {
    List<Class> types;
    List<Object> arguments;

    HarmonizedArgumentsAndTypes(List<Class> types, List<Object> arguments) {
      this.types = types;
      this.arguments = arguments;
    }
  }

  private Date parseStringDate(String stringDate) throws ParseException {
    return DATE_FORMAT.parse(stringDate.substring(0, 10));
  }

  HarmonizedArgumentsAndTypes generateTypeArray(Map<String, List<Object>> args) {
    // inputs
    List<Object> typeStrings = args.get("types");
    List<Object> rawValues = args.get("values");

    // outputs
    List<Object> newArguments = new ArrayList<>();
    List<Class> types = new ArrayList<>();

    int index = 0;
    for (Object arg: typeStrings) {
      String argument = arg.toString();
      switch (argument) {
        case "boolean":
          types.add(boolean.class);
          newArguments.add(rawValues.get(index));
          break;
        case "long":
          types.add(long.class);
          newArguments.add(rawValues.get(index));
          break;
        case "long[]":
          types.add(long[].class);
          List<Integer> values = (List<Integer>) rawValues.get(index);
          long[] newValues = new long[values.size()];
          for (int i = 0; i < values.size(); i++) {
            newValues[i] = values.get(i).longValue();
          }
          newArguments.add(newValues);
          break;
        case "double":
          types.add(double.class);
          newArguments.add(rawValues.get(index));
          break;
        case "Long":
          types.add(Long.class);
          newArguments.add(Long.valueOf((Integer) rawValues.get(index)));
          break;
        case "int":
          types.add(int.class);
          newArguments.add(rawValues.get(index));
          break;
        case "Map":
          types.add(Map.class);
          newArguments.add(rawValues.get(index));
          break;
        case "List":
          types.add(List.class);
          newArguments.add(rawValues.get(index));
          break;
        case "String[]":
          types.add(String[].class);
          newArguments.add(toStringArray((List<String>) rawValues.get(index)));
          break;
        case "Date":
          types.add(Date.class);
          newArguments.add(toDate((String) rawValues.get(index)));
          break;
        case "Date[]":
          types.add(Date[].class);
          newArguments.add(toDateArray((List<String>) rawValues.get(index)));
          break;
        case "Customer":
          types.add(Customer.class);
          newArguments.add(toCustomer((Map<String, String>) rawValues.get(index)));
          break;
        case "ProfileScope":
          types.add(Localytics.ProfileScope.class);
          newArguments.add(toProfileScope((String) rawValues.get(index)));
          break;
        case "Region":
          types.add(Region.class);
          newArguments.add(toRegion((Map<String, Object>) rawValues.get(index)));
          break;
        case "List<Region>":
          types.add(List.class);
          newArguments.add(toRegionList((List<Map<String, Object>>) rawValues.get(index)));
          break;
        case "Region.Event":
          types.add(Region.Event.class);
          newArguments.add(toRegionEvent((String) rawValues.get(index)));
          break;
        case "Location":
          types.add(Location.class);
          newArguments.add(toLocation((Map<String, Double>) rawValues.get(index)));
          break;
        case "InboxCampaign":
          types.add(InboxCampaign.class);
          Integer inboxCampaignId = (Integer) rawValues.get(index);
          newArguments.add(inboxCampaignCache.get(inboxCampaignId.longValue()));
          break;
        case "InAppCampaign":
          types.add(InAppCampaign.class);
          Integer inAppCampaignId = (Integer) rawValues.get(index);
          newArguments.add(inAppCampaignCache.get(inAppCampaignId.longValue()));
          break;
        case "PlacesCampaign":
          types.add(PlacesCampaign.class);
          Integer pushCampaignId = (Integer) rawValues.get(index);
          newArguments.add(pushCampaignCache.get(pushCampaignId.longValue()));
          break;
        case "ImpressionType":
          types.add(Localytics.ImpressionType.class);
          String rawImpressionType = (String) rawValues.get(index);
          newArguments.add(rawImpressionType.equals("click") ? Localytics.ImpressionType.CLICK : Localytics.ImpressionType.DISMISS);
          break;
        case "Context":
          types.add(Context.class);
          newArguments.add(context);
          break;
        default:
          Log.w("Localytics Flutter", "Received unknown type: " + argument);
          types.add(argument.getClass());
          newArguments.add(rawValues.get(index));
          break;
      }
      index++;
    }
    return new HarmonizedArgumentsAndTypes(types, newArguments);
  }

  private MessagingRelayer.InAppConfig toConfig(Map<String, Object> raw) {
    MessagingRelayer.InAppConfig c = new MessagingRelayer.InAppConfig();
    c.shouldShow = (Boolean) raw.get("shouldShow");
    c.diy = (Boolean) raw.get("diy");
    c.aspectRatio = (Double) raw.get("aspectRatio");
    c.backgroundAlpha = (Double) raw.get("backgroundAlpha");
    c.bannerOffsetDps = (Integer) raw.get("bannerOffsetDps");
    c.dismissButtonLocation = (String) raw.get("dismissButtonLocation");
    c.dismissButtonHidden = (Boolean) raw.get("dismissButtonHidden");
    c.videoConversionPercentage = (Double) raw.get("videoConversionPercentage");
    c.delaySessionStart = (Boolean) raw.get("delaySessionStart");
    return c;
  }

  private MessagingRelayer.PushConfig toPushConfig(Map<String, Object> raw) {
    MessagingRelayer.PushConfig c = new MessagingRelayer.PushConfig();
    c.shouldShow = (Boolean) raw.get("shouldShow");
    c.diy = (Boolean) raw.get("diy");
    c.category = (String) raw.get("category");
    c.color = (Integer) raw.get("color");
    c.contentInfo = (String) raw.get("contentInfo");
    c.contentTitle = (String) raw.get("contentTitle");
    c.defaults = (List<String>) raw.get("defaults");
    c.priority = (Integer) raw.get("priority");
    c.sound = (String) raw.get("sound");
    c.vibrate = (List<Integer>) raw.get("vibrate");
    return c;
  }

  private Location toLocation(Map<String, Double> map) {
    Location location = new Location("flutter");
    location.setLatitude(map.get("latitude"));
    location.setLongitude(map.get("longitude"));
    return location;
  }

  private Region.Event toRegionEvent(String event) {
    if (event.equals("enter")) {
      return Region.Event.ENTER;
    } else {
      return Region.Event.EXIT;
    }
  }

  private Region toRegion(Map<String, Object> map) {
    return new CircularRegion.Builder()
            .setUniqueId((String) map.get("uniqueId"))
            .build();
  }

  private List<Region> toRegionList(List<Map<String, Object>> list) {
    List<Region> regions = new ArrayList<>();
    for (Map<String, Object> map: list) {
      regions.add(toRegion(map));
    }
    return regions;
  }

  private Localytics.ProfileScope toProfileScope(String profileScope) {
    if (profileScope.equals("org")) {
      return Localytics.ProfileScope.ORGANIZATION;
    } else {
      return Localytics.ProfileScope.APPLICATION;
    }
  }

  private Customer toCustomer(Map<String, String> map) {
    Customer.Builder cb = new Customer.Builder();
    cb.setFirstName(map.get("firstName"));
    cb.setLastName(map.get("lastName"));
    cb.setFullName(map.get("fullName"));
    cb.setCustomerId(map.get("customerId"));
    cb.setEmailAddress(map.get("emailAddress"));
    return cb.build();
  }

  private Date toDate(String date) {
    try {
      return parseStringDate(date);
    } catch (ParseException e) {
      e.printStackTrace();
      return null;
    }
  }

  private Date[] toDateArray(List<String> dates) {
    Date[] dateArray = new Date[dates.size()];
    for (int i = 0; i< dates.size(); i++) {
      try {
        dateArray[i] = parseStringDate(dates.get(i));
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
    return dateArray;
  }

  private String[] toStringArray(List<String> strings) {
    String[] stringArray = new String[strings.size()];
    for (int i = 0; i < strings.size(); i++) {
      stringArray[i] = strings.get(i);
    }
    return stringArray;
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }
}
