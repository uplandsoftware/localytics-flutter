package com.localytics.localytics;

import android.content.Intent;
import android.os.Handler;

import androidx.annotation.NonNull;

import com.localytics.android.CallToActionListenerV2;
import com.localytics.android.Campaign;

import java.util.HashMap;
import java.util.Map;

import io.flutter.plugin.common.MethodChannel;

public class CallToActionRelayer implements CallToActionListenerV2 {

    private MethodChannel channel;
    private Handler handler;

    CallToActionRelayer(MethodChannel channel, Handler handler) {
        this.channel = channel;
        this.handler = handler;
    }

    @Override
    public boolean localyticsShouldDeeplinkToSettings(Intent intent, Campaign campaign) {
        final Map<String, Object> body = Translator.translate(campaign);
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsShouldDeeplinkToSettings", body);
            }
        });
        return true;
    }

    @Override
    public boolean localyticsShouldDeeplink(@NonNull String url, @NonNull Campaign campaign) {
        final Map<String, Object> body = new HashMap<>();
        body.put("campaign", Translator.translate(campaign));
        body.put("url", url);
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsShouldDeeplink", body);
            }
        });
        return true;
    }

    @Override
    public void localyticsDidOptOut(boolean optOut, @NonNull Campaign campaign) {
        final Map<String, Object> body = new HashMap<>();
        body.put("campaign", Translator.translate(campaign));
        body.put("optedOut", optOut);
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsDidOptOut", body);
            }
        });
    }

    @Override
    public void localyticsDidPrivacyOptOut(boolean optOut, @NonNull Campaign campaign) {
        final Map<String, Object> body = new HashMap<>();
        body.put("campaign", Translator.translate(campaign));
        body.put("optedOut", optOut);
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsDidPrivacyOptOut", body);
            }
        });
    }

    @Override
    public boolean localyticsShouldPromptForLocationPermissions(@NonNull Campaign campaign) {
        final Map<String, Object> body = Translator.translate(campaign);
        handler.post(new Runnable() {
            @Override
            public void run() {
                channel.invokeMethod("localyticsShouldPromptForLocationPermissions", body);
            }
        });
        return true;
    }
}
