package com.localytics.localytics;

import android.location.Location;
import android.net.Uri;

import com.localytics.android.Campaign;
import com.localytics.android.CircularRegion;
import com.localytics.android.InAppCampaign;
import com.localytics.android.InboxCampaign;
import com.localytics.android.Localytics;
import com.localytics.android.PlacesCampaign;
import com.localytics.android.PushCampaign;
import com.localytics.android.Region;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Translator {
    static Map<String, Double> translate(Location location) {
        Map<String, Double> raw = new HashMap<>();
        raw.put("latitude", location.getLatitude());
        raw.put("longitude", location.getLongitude());
        raw.put("altitude", location.getAltitude());
        raw.put("time", (double) (location.getTime() / 1000));
        raw.put("accuracy", (double) location.getAccuracy());
        return raw;
    }

    static Map<String, Object> translate(Region region) {
        Map<String, Object> raw = new HashMap<>();
        raw.put("name", region.getName());
        raw.put("uniqueId", region.getUniqueId());
        raw.put("type", region.getType());
        raw.put("latitude", region.getLatitude());
        raw.put("longitude", region.getLongitude());
        if (region instanceof CircularRegion) {
            raw.put("radius", ((CircularRegion) region).getRadius());
        }
        raw.put("attributes", region.getAttributes());
        return raw;
    }

    static List<Map<String, Object>> translate(List<? extends Region> regions) {
        List<Map<String, Object>> rawRegions = new ArrayList<>();
        for (Region region : regions) {
            rawRegions.add(Translator.translate(region));
        }
        return rawRegions;
    }

    static String translate(Region.Event event) {
        switch (event) {
            case ENTER:
                return "enter";
            default:
                return "exit";
        }
    }

    public static Map<String, Object> translate(Campaign campaign) {
        if (campaign instanceof PlacesCampaign) {
            return translate((PlacesCampaign) campaign);
        } else if (campaign instanceof PushCampaign) {
            return translate((PushCampaign) campaign);
        } else if (campaign instanceof InboxCampaign) {
            return translate((InboxCampaign) campaign);
        } else if (campaign instanceof InAppCampaign) {
            return translate((InAppCampaign) campaign);
        }
        return null; // should never happen
    }

    public static Map<String, Object> translate(InAppCampaign campaign) {
        Map<String, Object> raw = new HashMap<>();
        // Campaign
        raw.put("campaignId", campaign.getCampaignId());
        raw.put("name", campaign.getName());
        raw.put("attributes", campaign.getAttributes());
        // WebViewCampaign
        Uri creativeFilePath = campaign.getCreativeFilePath();
        raw.put("creativeFilePath", creativeFilePath != null ? creativeFilePath.toString() : "");

        // InAppCampaign
        if (!Double.isNaN(campaign.getAspectRatio())) {
            raw.put("aspectRatio", campaign.getAspectRatio());
        }
        raw.put("bannerOffsetDps", campaign.getOffset());
        raw.put("backgroundAlpha", campaign.getBackgroundAlpha());
        raw.put("displayLocation", campaign.getDisplayLocation());
        raw.put("dismissButtonHidden", campaign.isDismissButtonHidden());
        if (Localytics.InAppMessageDismissButtonLocation.RIGHT.equals(campaign.getDismissButtonLocation())) {
            raw.put("dismissButtonLocation", "right");
        } else {
            raw.put("dismissButtonLocation", "left");
        }
        raw.put("eventName", campaign.getEventName());
        raw.put("eventAttributes", campaign.getEventAttributes());
        raw.put("classType", "InAppCampaign");

        return raw;
    }

    static Localytics.InAppMessageDismissButtonLocation translateDismissButtonLocation(String location) {
        if ("right".equalsIgnoreCase(location)) {
            return Localytics.InAppMessageDismissButtonLocation.RIGHT;
        } else {
            return Localytics.InAppMessageDismissButtonLocation.LEFT;
        }
    }

    public static Map<String, Object> translate(PushCampaign campaign) {
        Map<String, Object> raw = new HashMap<>();

        // Campaign
        raw.put("campaignId", (int) campaign.getCampaignId());
        raw.put("name", campaign.getName());
        raw.put("attributes", campaign.getAttributes());

        // PushCampaign
        raw.put("title", campaign.getTitle());
        raw.put("creativeId", (int) campaign.getCreativeId());
        raw.put("creativeType", campaign.getCreativeType());
        raw.put("message", campaign.getMessage());
        raw.put("soundFilename", campaign.getSoundFilename());
        raw.put("attachmentUrl", campaign.getAttachmentUrl());
        raw.put("classType", "PushCampaign");

        return raw;
    }

    public static Map<String, Object> translate(PlacesCampaign campaign) {
        Map<String, Object> raw = new HashMap<>();

        // Campaign
        raw.put("campaignId", (int) campaign.getCampaignId());
        raw.put("name", campaign.getName());
        raw.put("attributes", campaign.getAttributes());

        // PlacesCampaign
        raw.put("title", campaign.getTitle());
        raw.put("creativeId", (int) campaign.getCreativeId());
        raw.put("creativeType", campaign.getCreativeType());
        raw.put("message", campaign.getMessage());
        raw.put("soundFilename", campaign.getSoundFilename());
        raw.put("attachmentUrl", campaign.getAttachmentUrl());
        raw.put("region", translate(campaign.getRegion()));
        if (Region.Event.ENTER.equals(campaign.getTriggerEvent())) {
            raw.put("triggerEvent", "enter");
        } else {
            raw.put("triggerEvent", "exit");
        }
        raw.put("classType", "PlacesCampaign");

        return raw;
    }
    public static Map<String, Object> translate(InboxCampaign c) {
        Map<String, Object> r = new HashMap<>();
        r.put("campaignId", c.getCampaignId());
        r.put("ruleName", c.getName());
        r.put("attributes", c.getAttributes());
        r.put("read", c.isRead());
        r.put("visible", c.isVisible());
        r.put("deleted", c.isDeleted());
        r.put("pushToInboxCampaign", c.isPushToInboxCampaign());
        r.put("title", c.getTitle());
        r.put("sortOrder", c.getSortOrder());
        r.put("receivedDate", c.getReceivedDate().toString());
        r.put("summary", c.getSummary());
        r.put("hasThumbnail", c.hasThumbnail());
        Uri uri = c.getThumbnailUri();
        if (uri != null) {
            r.put("thumbnailUri", uri.toString());
        }
        r.put("hasCreative", c.hasCreative());
        r.put("deepLinkUrl", c.getDeepLinkUrl());
        r.put("classType", "InboxCampaign");
        return r;
    }
}
