import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:localytics_flutter/localytics.dart';

void main() {
  const MethodChannel channel = MethodChannel('localytics');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  // test('getPlatformVersion', () async {
  //   expect(await Localytics.platformVersion, '42');
  // });
}
