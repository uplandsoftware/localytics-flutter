package com.localytics.localytics_example;

import com.localytics.android.Localytics;

import io.flutter.app.FlutterApplication;

public class MyApplication extends FlutterApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Localytics.autoIntegrate(this);
        Localytics.setOption("ll_region_throttle_time", 0);
        Localytics.setOption("ll_min_region_dwell_time", 0);
        Localytics.registerPush();
    }
}
