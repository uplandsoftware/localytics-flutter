import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:localytics_flutter/localytics.dart';

class RouteAwareWidget extends StatefulWidget {
  RouteObserver<PageRoute> routeObserver;
  RouteAwareWidget(RouteObserver routeObserver) {
    this.routeObserver = routeObserver;
  }
  State<RouteAwareWidget> createState() => RouteAwareWidgetState(routeObserver);
}

// Implement RouteAware in a widget's state and subscribe it to the RouteObserver.
class RouteAwareWidgetState extends State<RouteAwareWidget> with RouteAware {
  RouteObserver<PageRoute> routeObserver;
  RouteAwareWidgetState(RouteObserver routeObserver) {
    this.routeObserver = routeObserver;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPush() {
    // Route was pushed onto navigator and is now topmost route.
    print("pushed screen");

    Future.delayed(const Duration(seconds: 5), () {
      // Here you can write your code
      Localytics.tagEvent("an event");

      setState(() {
        // Here you can write your code for open new view
      });
    });
  }

  @override
  void didPopNext() {
    // Covering route was popped off the navigator.
  }

  @override
  Widget build(BuildContext context) => Container();
}
