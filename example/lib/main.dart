import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:localytics_flutter/localytics.dart';
import 'package:localytics_flutter/AnalyticsListener.dart';
import 'package:localytics_flutter/CallToActionListener.dart';
import 'package:localytics_flutter/Campaign.dart';
import 'package:localytics_flutter/CircularRegion.dart';
import 'package:localytics_flutter/Customer.dart';
import 'package:localytics_flutter/ImpressionType.dart';
import 'package:localytics_flutter/InAppCampaign.dart';
import 'package:localytics_flutter/InAppConfiguration.dart';
import 'package:localytics_flutter/InboxCampaign.dart';
import 'package:localytics_flutter/MessagingListener.dart';
import 'package:localytics_flutter/PushCampaign.dart';
import 'package:localytics_flutter/LocationListener.dart';
import 'package:localytics_flutter/Location.dart';
import 'package:localytics_example/WidgetExample.dart';
import 'package:localytics_flutter/RegionEvent.dart';
import 'package:localytics_flutter/ProfileScope.dart';

void main() {
  runApp(MyApp());
  Localytics.setAnalyticsListener(new ALImplementation());
  Localytics.setLocationListener(new LLImplementation());
  Localytics.setCallToActionListener(new CLImplementation());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with RouteAware {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      InAppConfiguration inAppConfig = new InAppConfiguration();
      inAppConfig.dismissButtonHidden = true;
      Localytics.setMessagingListener(
          new MLImplementation(), inAppConfig, null, null);
      // Localytics.tagEvent("an event");
      // Localytics.openSession();
      // Localytics.closeSession();
      // Localytics.pauseDataUploading(false);
      Localytics.tagEventWithAttributes('attributed event',
          <String, String>{'attribute1': 'value1', 'attribute2': 'value2'});
      Localytics.tagEventWithAttributesAndValueIncrease('attributed event',
          <String, String>{'attribute1': 'value1', 'attribute2': 'value2'}, 11);
      Localytics.tagPurchased('itemName', 'itemId', null, 100,
          <String, String>{'attribute1': 'value1', 'attribute2': 'value2'});
      Localytics.tagAddedToCart('itemName', 'itemId', 'some string', 100,
          <String, String>{'attribute1': 'value1', 'attribute2': 'value2'});
      Localytics.tagStartedCheckout(100, 2,
          null); //<-- crashes on iOS, internal SDK error //NSNull vs nil issues?
      Localytics.tagCompletedCheckout(
          100, 2, null); //<-- crashes on iOS, internal SDK error
      Localytics.tagContentViewed(
          'content name', 'content ID', 'content Type', <String, String>{
        'attribute1': 'value1',
        'attribute2': 'value2'
      }); //<-- crashes on iOS, internal SDK error
      Localytics.tagSearched('some query', 'some content', 23, null);
      Localytics.tagShared(
          'content', 'contentId', 'contentType', 'methodName', null);
      Localytics.tagContentRated(
          'contentName', 'contentId', 'contentType', 5, null);
      Customer c = new Customer();
      c.firstName = 'John';
      c.lastName = 'Nilsen';
      c.customerId = 'jn1';
      Localytics.tagCustomerRegistered(c, 'web', null);
      Localytics.tagCustomerLoggedIn(c, 'web', null);
      Localytics.tagCustomerLoggedOut(null);
      Localytics.tagInvited('methodName', null);
      Localytics.tagScreen('screenName');
      Localytics.setCustomDimension(1, 'value');
      String value = await Localytics.getCustomDimension(1);
      print('got custom dimension: $value');

      Localytics.setNumericProfileAttribute('attr1', 22,
          scope: ProfileScope.organization);
      Localytics.setNumericProfileAttribute('attr2', 33);
      Localytics.setNumericArrayProfileAttribute('attr3', [1, 2, 3],
          scope: ProfileScope.organization);
      Localytics.setNumericArrayProfileAttribute('attr4', [4, 5, 6]);
      Localytics.setProfileAttribute('attr5', 'value1');
      Localytics.setProfileAttribute('attr6', 'value2',
          scope: ProfileScope.organization);
      Localytics.setArrayProfileAttribute('attr7', ['one', 'two'],
          scope: ProfileScope.organization);
      Localytics.setDateProfileAttribute('attr8', DateTime.now());
      Localytics.setDateArrayProfileAttribute('attr9',
          [new DateTime.utc(1989, 11, 9), new DateTime.utc(1989, 11, 10)]);

      Localytics.addNumericArrayProfileAttributeToSet('attr1', [11, 22, 33]);
      Localytics.addArrayProfileAttributeToSet('attr5', ['v1', 'v2', 'v3']);
      Localytics.addDateArrayProfileAttributeToSet('attr8', [DateTime.now()]);

      Localytics.removeNumericArrayProfileAttributeFromSet(
          'attr1', [11, 22, 33]);
      Localytics.removeArrayProfileAttributeFromSet(
          'attr5', ['v1', 'v2', 'v3']);
      Localytics.removeDateArrayProfileAttributeFromSet(
          'attr8', [DateTime.now()]);

      Localytics.incrementNumericProfileAttribute('attr2', 22);
      Localytics.decrementNumericProfileAttribute('attr2', 22);
      Localytics.deleteProfileAttribute('attr2');

      Localytics.setCustomerEmail('user@localytics.com');
      Localytics.setCustomerFirstName('John');
      Localytics.setCustomerLastName('Doe');
      Localytics.setCustomerFullName('John Doe');

      Localytics.triggerInAppMessage('message');
      Localytics.triggerInAppMessageWithAttributes(
          'message2', <String, String>{'attr1': 'v1', 'attr2': 'v2'});
      Localytics.triggerInAppMessagesForSessionStart();
      // Localytics.dismissCurrentInAppMessage();

      // Localytics.registerPush();
      // Localytics.setPushRegistrationId('id123');
      String registrationId = await Localytics.getPushRegistrationId();
      print("registration id: $registrationId");
      if (defaultTargetPlatform == TargetPlatform.android) {
        LocalyticsAndroid.tagPushReceivedEvent(
            <String, String>{'attr1': 'v1', 'attr2': 'v2'});
        LocalyticsAndroid.setNotificationsDisabled(false);
        bool areDisabled = await LocalyticsAndroid.areNotificationsDisabled();
        print("are notifications disabled: $areDisabled");

        LocalyticsAndroid.appendAdidToInAppUrls(true);
        bool isAdidAppendedToUrls =
            await LocalyticsAndroid.isAdidAppendedToInAppUrls();
        print("adid appended to URLs: $isAdidAppendedToUrls");
        LocalyticsAndroid.appendAdidToInboxUrls(true);
        bool isAdidAppendedToInbox =
            await LocalyticsAndroid.isAdidAppendedToInboxUrls();
        print("adid appended to Inbox: $isAdidAppendedToInbox");
      }

      // Localytics.setTestModeEnabled(true);
      // bool isTestModeEnabled = await Localytics.isTestModeEnabled();
      // print("test mode enabled: $isTestModeEnabled");
      // Localytics.enableLiveDeviceLogging();

      List<CircularRegion> regions =
          await Localytics.getGeofencesToMonitor(0, 0);
      int regionCount = regions.length;
      print("got back $regionCount regions");

      Localytics.triggerRegion(regions[1], RegionEvent.enter, Location(0, 0));
      Localytics.triggerRegions(regions, RegionEvent.exit, Location(40, 40));

      Localytics.setCustomerId('123id');
      Localytics.setIdentifier('my key', 'some value');
      Localytics.setCustomerIdWithPrivacyOptedOut('123id', false);
      String customerId = await Localytics.getCustomerId();
      print("got back customerId: $customerId");
      String customIdentifier = await Localytics.getIdentifier('my key');
      print("got back custom identifier: $customIdentifier");
      Localytics.setLocation(Location(30, 30));

      List<InboxCampaign> inboxCampaigns =
          await Localytics.getAllInboxCampaigns();
      int inboxCampaignCount = inboxCampaigns.length;
      print("got back $inboxCampaignCount inbox campaigns");
      Localytics.setInboxCampaignRead(inboxCampaigns[0], false);
      Localytics.tagInboxImpressionWithCustomAction(
          inboxCampaigns[0], 'customAction');
      Localytics.tagInBoxImpression(inboxCampaigns[0], ImpressionType.dismiss);
      Localytics.tagPushToInboxImpression(inboxCampaigns[0]);
      int inboxUnreadCount = await Localytics.getInboxCampaignUnreadCount();
      print("unread inbox campaigns: $inboxUnreadCount");
      Localytics.inboxListItemTapped(inboxCampaigns[0]);
      // Localytics.deleteInboxCampaing(inboxCampaigns[0]);
      // Localytics.redirectLogsToDisk(true);

      // if (defaultTargetPlatform == TargetPlatform.iOS) {
      //   Localytics.requestAdvertisingIdentifierPrompt();
      //   Localytics.advertisingIdentifierStatus();
      // }

      Localytics.setOptedOut(false);
      Localytics.setPrivacyOptedOut(false);

      Localytics.isOptedOut();
      Localytics.isPrivacyOptedOut();

      // Localytics.upload();
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: RouteAwareWidget(routeObserver),
        ),
      ),
      navigatorObservers: [routeObserver],
    );
  }
}

class ALImplementation extends AnalyticsListener {
  void localyticsSessionWillOpen(bool isFirst, bool isUpgrade, bool isResume) {
    print("session will open");
  }

  void localyticsSessionDidOpen(bool isFirst, bool isUpgrade, bool isResume) {
    print("session did open");
  }

  void localyticsSessionWillClose() {
    print("session will close");
  }

  void localyticsDidTagEvent(String eventName, Map<String, String> attributes,
      int customerValueIncrease) {
    print("did tag event $eventName");
  }
}

class LLImplementation extends LocationListener {
  void localyticsDidUpdateLocation(Location location) {
    double latitude = location.latitude;
    double longitude = location.longitude;
    print("updated location $latitude $longitude");
  }

  void localyticsDidTriggerRegions(
      List<CircularRegion> regions, RegionEvent event) {
    int count = regions.length;
    String eventName = event.rawValue();
    print("triggered $count regions with an $eventName event");
  }

  void localyticsDidUpdateMonitoredGeofences(
      List<CircularRegion> added, List<CircularRegion> removed) {
    int addedCount = added.length;
    int removedCount = removed.length;
    print(
        "did update monitored geofences, added: $addedCount removed: $removedCount");
  }
}

class MLImplementation extends MessagingListener {
  void localyticsShouldShowInAppMessage(InAppCampaign campaign) {
    print("localyticsShouldShowInAppMessage");
  }

  void localyticsWillDisplayInAppMessage(InAppCampaign campaign) {
    print("localyticsWillDisplayInAppMessage");
    Localytics.tagInAppImpression(campaign, ImpressionType.click);
    Localytics.tagInAppImpressionWithCustomAction(campaign, "customAction");
  }

  void localyticsDidDisplayInAppMessage() {
    print("localyticsDidDisplayInAppMessage");
  }

  void localyticsWillDismissInAppMessage() {
    print("localyticsWillDismissInAppMessage");
  }

  void localyticsDidDismissInAppMessage() {
    print("localyticsDidDismissInAppMessage");
  }

  void localyticsShouldDelaySessionStartInAppMessages(bool shouldDelay) {
    print("localyticsShouldDelaySessionStartInAppMessages");
  }

  void localyticsShouldShowPushNotification(PushCampaign campaign) {
    print("localyticsShouldShowPushNotification");
  }

  void localyticsWillShowPushNotification(PushCampaign campaign) {
    print("localyticsWillShowPushNotification");
  }
}

class CLImplementation extends CallToActionListener {
  void localyticsShouldDeeplinkToSettings(Campaign campaign) {
    print('localyticsShouldDeeplinkToSettings');
  }

  void localyticsShouldDeeplink(String url, Campaign campaign) {
    print('localyticsShouldDeeplink');
  }

  void localyticsDidOptOut(bool optOut, Campaign campaign) {
    print('localyticsDidOptOut');
  }

  void localyticsDidPrivacyOptOut(bool optOut, Campaign campaign) {
    print('localyticsDidPrivacyOptOut');
  }

  void localyticsShouldPromptForLocationPermissions(Campaign campaign) {
    print('localyticsShouldPromptForLocationPermissions');
  }
}
