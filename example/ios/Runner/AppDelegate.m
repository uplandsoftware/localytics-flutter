#import "AppDelegate.h"
#import "GeneratedPluginRegistrant.h"
#import "MyLocationManagerDelegate.h"
@import Localytics;
@import UserNotifications;

@implementation AppDelegate

MyLocationManagerDelegate *lm;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Push configuration
    [application registerForRemoteNotifications];
    if (NSClassFromString(@"UNUserNotificationCenter") && @available(iOS 12.0, *)) {
        if (@available(iOS 12.0, *)) {
            UNAuthorizationOptions options = UNAuthorizationOptionProvisional;
            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:options
                                                                                completionHandler:^(BOOL granted, NSError * _Nullable error) {
                [Localytics didRequestUserNotificationAuthorizationWithOptions:options
                                                                       granted:granted];
            }];
        } else {
            // Fallback on earlier versions
        }
    }
    if (NSClassFromString(@"UNUserNotificationCenter")) {
      UNAuthorizationOptions options = (UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert);
      [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:options
                                                                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                                                            [Localytics didRequestUserNotificationAuthorizationWithOptions:options
                                                                                                                                   granted:granted];
                                                                          }];
    } else {
      UIUserNotificationType types = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
      UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
      [application registerUserNotificationSettings:settings];
    }
    
    
    
    // flutter configuration
    [GeneratedPluginRegistrant registerWithRegistry:self];
    
    
    // Localytics configuration
    [Localytics autoIntegrate:@"53e5bfd7badf5ec0e452686-0ae8eaf4-1ebb-11eb-ec5d-007c928ca240"
        withLocalyticsOptions:@{
            LOCALYTICS_WIFI_UPLOAD_INTERVAL_SECONDS: @5,
            LOCALYTICS_GREAT_NETWORK_UPLOAD_INTERVAL_SECONDS: @10,
            LOCALYTICS_DECENT_NETWORK_UPLOAD_INTERVAL_SECONDS: @30,
            LOCALYTICS_BAD_NETWORK_UPLOAD_INTERVAL_SECONDS: @90
        }
                launchOptions:launchOptions];
    
    //    +        Localytics.setOption("ll_region_throttle_time", 0);
    //    +        Localytics.setOption("ll_min_region_dwell_time", 0);
    [Localytics setOptions:@{@"min_region_dwell_time": @0, @"region_throttle_time": @0}];
    
    printf("was here\n");
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

@end
