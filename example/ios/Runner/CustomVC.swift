//
//  CustomVC.swift
//  Runner
//
//  Created by John Rikard Nilsen on 20.10.20.
//

import CoreLocation
import Flutter
import Foundation

class CustomVC: FlutterViewController {
    let lm = CLLocationManager()
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        lm.delegate = self
        lm.requestAlwaysAuthorization()
        lm.requestWhenInUseAuthorization()
    }
}

extension CustomVC: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if #available(iOS 14.0, *) {
            switch manager.authorizationStatus {
            case .authorizedAlways: fallthrough
            case .authorizedWhenInUse:
                lm.desiredAccuracy = kCLLocationAccuracyBest
                lm.distanceFilter = kCLDistanceFilterNone
                lm.startUpdatingLocation()
            default:
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("locations updated! \(locations.first!)")
    }
}
