//
//  MyLocationManagerDelegate.m
//  Runner
//
//  Created by John Rikard Nilsen on 20.10.20.
//

#import <Foundation/Foundation.h>
#import "MyLocationManagerDelegate.h"
#import <CoreLocation/CoreLocation.h>

@implementation MyLocationManagerDelegate : NSObject


- (void) locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
{
    //This method will show us that we recieved the new location
    NSLog(@"Latitude = %f",newLocation.coordinate.latitude );
    NSLog(@"Longitude =%f",newLocation.coordinate.longitude);

}

@end
