//
//  MyLocationManagerDelegate.h
//  Runner
//
//  Created by John Rikard Nilsen on 20.10.20.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MyLocationManagerDelegate: NSObject <CLLocationManagerDelegate>
@property (nonatomic,strong)CLLocationManager * myLocationManger;
@end
