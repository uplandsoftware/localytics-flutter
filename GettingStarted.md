# Getting Started with Localytics in Flutter
---
This  Flutter plugin simplifies the process of integrating Localytics into Flutter iOS and Android projects.

The steps to integrate Localytics with this component mirrors the native Localytics SDK, so the following docs for the respective platforms maybe helpful:

* [iOS Localytics Docs](http://docs.localytics.com/dev/ios.html)
* [Android Localytics Docs](http://docs.localytics.com/dev/android.html)


## Getting Started

### Install the SDK
To install the SDK add this to your pubspec.yaml:
```
//TODO: add pub.dev dependency here
```

## API Summary

### Common APIs

| API                    | Description  |
|------------------------|------|
| openSession  | Opens a session. Multiple calls are coallesed.  |
| closeSession | Open Sessions are marked with a pending close. Sessions are extended if there is localytics activity before expiry of Session timer |
| upload| uploads any data stored on the device by the localytics SDK. Essential to do this early, to ensure upload completes before app is suspended.|
| pauseDataUploading | all data upload is deferred until it is resumed. Calls to the upload API dont perform any action. When data upload is resumed, all locally stored data is immediately uploaded. |
| tagEvent | Tag an event |
| tagPurchased | A standard event to tag a single item purchase event (after the action has occurred) |
| tagAddedToCart | A standard event to tag the addition of a single item to a cart (after the action has occurred) |
| tagStartedCheckout | A standard event to tag the start of the checkout process (after the action has occurred) |
| tagCompletedCheckout | A standard event to tag the conclusions of the checkout process (after the action has occurred) |
| tagContentViewed | A standard event to tag the viewing of content (after the action has occurred) |
| tagSearched | A standard event to tag a search event (after the action has occurred) |
| tagShared | A standard event to tag a share event (after the action has occurred) |
| tagContentRated | A standard event to tag the rating of content (after the action has occurred) |
| tagCustomerRegistered | A standard event to tag the registration of a user (after the action has occurred) |
| tagCustomerLoggedIn | A standard event to tag the logging in of a user (after the action has occurred) |
| tagCustomerLoggedOut | A standard event to tag the logging out of a user (after the action has occurred) |
| tagInvited | A standard event to tag the invitation of a user (after the action has occured) |
| tagScreen | Allows tagging the flow of screens encountered during the session. |
| setCustomDimension | Sets the value of custom dimension which is user defined data. Customer sensitive data should be hashed or encrypted |
| getCustomDimension |Gets the custom value for a given dimension. Must not be called from the main thread. |
| setIdentifier | Sets the value of a custom identifier |
| getIdentifier | Gets the identifier value for a given identifier. Must not be called form the main thread. |
| customerId | property representing a customer Id. Recommended to use SetCustomerId. privacy sensitive data should be hashed or encyrpted |
| setCustomerId | set customer Id and privacy status automically.|
| setProfileAttribute | Attribute values can be long, string, Array of long or Array of String |
| addProfileAttribute | Adds values to a profile attribute that is a set |
| removeProfileAttribute | Removes values from a profile attribute that is a set |
| incrementProfileAttribute | Increment the value of a profile attribute. |
| decrementProfileAttribute | Decrement the value of a profile attribute. |
| deleteProfileAttribute | Delete a profile attribute |
| setCustomerEmail | Convenience method to set a customer's email |
| setCustomerFirstName | Convenience method to set a customer's first name |
| setCustomerLastName | Convenience method to set a customer's last name |
| setCustomerFullName | Convenience method to set a customer's full name |
| setOptions | Customize the behavior of the SDK by setting custom values for various options.|
| setOption | Customize the behavior of the SDK by setting custom value for various options.|
| setLoggingEnabled |property that controls if the localytics SDK emits logging information. |
| optedOut | control collection of user data. |
| privacyOptedOut | Opts out of data collection and requests a Delete data request to be submitting to the cloud service. |
| installId | An Installtion Identifier |
| libraryVersion | version of the Localytics SDK |
| testModeEnabled | Controls the Test Mode charactertistics of the Localytics SDK |
| inAppAdIdParameterEnabled | ADID parameter is added to In-App call to action URLs |
| triggerPlacesNotificationForCampaignId | Trigger a places notification for the given campaign id and regionId |
| inboxAdIdParameterEnabled | ADID parameter is added to Inbox call to action URLs |
| inAppMessageDismissButtonLocation | location of the dismiss button on an In-App msg |
| setInAppMessageDismissButtonHidden | dismiss button hidden state on an In-App message |
| triggerInAppMessage | Trigger an In-App message |
| triggerInAppMessagesForSessionStart | Trigger campaigns as if a Session Start event had just occurred. |
| dismissCurrentInAppMessage | Dismiss a currently displayed In-App message. |
| inboxCampaigns | an array of all Inbox campaigns that are enabled and can be displayed |
| allInboxCampaigns | an array of all Inbox campaigns that are enabled. |
| refreshInboxCampaigns | Refresh inbox campaigns from the Localytics server that are enabled and can be displayed. |
| refreshAllInboxCampaigns | Refresh inbox campaigns from the Localytics server that are enabled. |
| tagImpression | A standard event to tag an In-App impression. |
| setInboxCampaign | Set an Inbox campaign as read. |
| inboxListItemTapped | Tell the Localytics SDK that an Inbox campaign was tapped in the list view.  |
| inboxCampaignsUnreadCount | count of unread inbox messages |
| setLocationMonitoringEnabled | Enable or disable location monitoring for geofence monitoring | 
| pushTokenInfo | return a string version of Push Token on all platforms.| 

### Dictionaries

#### Customer Properties Dictionary

| Dictionary Key Name    | Value Description  |
|------------------------|-------|
| customerId | Customer Id |
| firstName  | First Name of the customer |
| lastName   | Last Name of the customer  |
| fullName   | Full Name of the customer  |
| emailAddress | Email Address of the customer |

This Dictionary can be passed to tagCustomerLoggedIn and tagCustomerRegistered API.

### Callbacks
For comprenesive documentation on the callbacks please refer to both android and iOS documentation.

#### Analytics callbacks
These analytics callbacks are useful for setting the value of a custom dimension or profile attribute before a session opens, firing an event at the beginning or end of a session, or taking action in your app based on an auto-tagged campaign performance event in the Localytics SDK.

#### Messaging callbacks
Messaging callbacks are useful for understanding when Localytics will display messaging campaigns. This can help you prevent conflicts with other views in your app, as well as potentially suppress the Localytics display.

#### Location callbacks
Location callbacks are useful for understanding when Localytics responds location changes.

#### Call to action callbacks
Call to action callbacks are useful for understanding when Localytics has triggered a deeplink or internal event through a javascript API.

### Android Versions

| Type  |  Version    |
|-------|-------------|
| Minimum Android Version | API Level 21 |
| Target Android Version  | API Level 28 |
| Target Framework        | Android 9.0 |

### IOS Versions
* Minimum Deployment Target is IOS 9.0
* CoreLocation - Required for Location Services; Requires implementing the request for Location Permissions in the App


### Change Log
* 6.2.0 - Native SDK 6.2

## iOS
In your iOS app delegate you will have to call `integrate` or `autoIntegrate` on `didFinishLaunchingWithOptions`.

```
    [Localytics autoIntegrate:@"<YOUR LOCALYTICS APP KEY>"
        withLocalyticsOptions:nil                
	        launchOptions:launchOptions];
```
### Usage
---
Anywhere in your application, you can use the Localytics API by calling the class methods/accessors of `Localytics`. Here's a sample of some of the calls.

```
    Localytics.setOption("ll_session_timeout_seconds", 10);
    Localytics.setCustomerId("Sample Customer");

    Localytics.setProfileAttribute("Sample Attribute", 83);

    Localytics.tagEvent("Test Event");
    Localytics.tagScreen("Test Screen");

    Localytics.upload();
```

The above only demonstrate the syntax of calling the Flutter API.  For more information about how to use Localytics, please refer to [iOS Localytics Docs](http://docs.localytics.com/dev/ios.html).

### Push & In-App Messaging
---
1. Enable background modes

  * Under Project Options -> Build -> iOS Application -> Background Modes, turn on Enable Background Modes and Remote notfication. You can also edit this in the `info.plist`.

2. Register for remote notifications

  Add the following in your main AppDelegate inside `FinishedLaunching` where the `autoIntegrate` call is

  ```
  if (NSClassFromString(@"UNUserNotificationCenter") && @available(iOS 12.0, *)) {
  UNAuthorizationOptions options = UNAuthorizationOptionProvisional;
  [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:options
            completionHandler:^(BOOL granted, NSError * _Nullable error) {
                [Localytics didRequestUserNotificationAuthorizationWithOptions:options
                            granted:granted];
  }];
}
// the following will request users to opt in for push messages
if (NSClassFromString(@"UNUserNotificationCenter")) {
  UNAuthorizationOptions options = (UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert);
  [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:options
                                                                      completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                                                        [Localytics didRequestUserNotificationAuthorizationWithOptions:options
                                                                                                                               granted:granted];
                                                                      }];
} else {
  UIUserNotificationType types = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
  UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
  [application registerUserNotificationSettings:settings];
}
  ```
  
## android
---

Call one of the Integrate methods `integrate` or `autoIntegrate` within your custom `Application` class
  
  ```
  public class MyApplication extends FlutterApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Localytics.autoIntegrate(this);
    }
}
  ```

Modify the AndroidManifest.xml to add App Key, receivers and permissions.
  
  ```
  <application android:label="LocalyticsSample" android:icon="@drawable/icon">
      <meta-data android:name="LOCALYTICS_APP_KEY" android:value="xxxxxxxxxxxxxxxxxxxxxxx-xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx" />
      <receiver android:name="com.localytics.android.ReferralReceiver" android:exported="true">
          <intent-filter>
              <action android:name="com.android.vending.INSTALL_REFERRER" />
          </intent-filter>
      </receiver>
  </application>
  <uses-permission android:name="android.permission.INTERNET" />
  <uses-permission android:name="android.permission.WAKE_LOCK" />
  ```

Configure required parameters using localytics.xml or through setting options
	* Add a localytics.xml in Resources/values. For a sample refer [here](LocalyticsXamarin/Android/Resources/values/localytics.xml)
        

### Usage
---
Anywhere in your application, you can use the Localytics API by calling the class methods/accessors of `Localytics`. Here's a sample of some of the calls.

```
    Localytics.setOption("ll_session_timeout_seconds", 10);
    Localytics.setCustomerId("Sample Customer");

    Localytics.setProfileAttribute("Sample Attribute", 83);

    Localytics.tagEvent("Test Event");
    Localytics.tagScreen("Test Screen");

    Localytics.upload();
```


### Push & In-App Messaging
---

Configure your android pushes like you usually would with Firebase. In your main activity add this:

```
public class MainActivity extends FlutterActivity {
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Localytics.onNewIntent(this, intent);
    }
// ...
}
```
